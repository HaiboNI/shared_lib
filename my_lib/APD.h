/*

    simple function to measure the APDs

    Haibo Ni

    qiangzi.ni@gmail.com
    last update: Jan. 12, 3014
    by Haibo Ni
*/

#ifndef APD_H
#define APD_H

#include <stdio.h>

typedef struct {
    double Vmax, Vmin, timeAPDstart, timeAPDend, dVdtmax, APD_out_end[2];
    int APD_switch, APD_count, Vmax_switch, APD_out_swhich;
    double APD90, APD50, APD30, APD75;
    double Vm_prev;
    double v90, v75, v50, v30;
    double ICaL_in, RyR_in;
    double Istim_prev;
    double AMP, AMP_last;
    double APD90_prev;
    double t_since_up_stroke;
    FILE *out;
} AP_infor;

void AP_information_initialise(AP_infor *AP);
void APD90_measure(double t, double Istim, double BCL, double dt, double Vm, AP_infor *AP, FILE *out);
void APD90_measure(double t, double Istim, double BCL, double dt, double Vm, AP_infor *AP, FILE *out);
void calcium_accumulation(double t, double Istim, double BCL, double dt, double ICaL, double J_RyR, AP_infor *AP, FILE *out);



void AP_information_initialise(AP_infor *AP) {
    AP->Vmax = 0.0;
    AP->Vmin = 0.0;
    AP->timeAPDstart = 0.0;
    AP->timeAPDend = 0.0;
    AP->dVdtmax = 0.0;
    AP->APD_out_end[0] = 0.0;
    AP->APD_out_end[1] = 0.0;

    AP->APD_switch = 0;
    AP->APD_count = 0;
    AP->Vmax_switch = 0;
    AP->APD_out_swhich = 0;
    AP->APD90 = AP->APD75 = AP->APD50 = AP->APD30 = 0.0;
    AP->Vm_prev = 0.0;
    AP->v90 = 0.0;
    AP->v75 = 0.0;
    AP->v50 = 0.0;
    AP->v30 = 0.0;
    AP->ICaL_in = 0.0;
    AP->RyR_in = 0.0;
    AP->Istim_prev = 0.0;
    AP->AMP = 0.0;
    AP->AMP_last = 0.0;
    AP->APD90_prev = 0.0;
    AP->t_since_up_stroke = 0.0;

}

void APD90_measure(double t, double Istim, double BCL, double dt, double Vm, AP_infor *AP, FILE *out)  {

    if (((Istim >= 1e-3 || Istim <= -1e-3 ) && AP->APD_switch == 0)) {
        AP->APD_switch = 1;
        AP->Vmax = -80;
        AP->timeAPDstart = t;
        AP->Vmin = Vm;
        AP->dVdtmax = 0.0;
        AP->t_since_up_stroke = 0.0;
        AP->v90 = 0.0;
        AP->v75 = 0.0;
        AP->v50 = 0.0;
        AP->v30 = 0.0;
        // printf("ssssssssss\n");
    }

    if (AP->APD_switch == 1) {
        AP->t_since_up_stroke += dt;
        if (Vm > -75.0) {
            if ((Vm > AP->Vmax) && (AP->t_since_up_stroke <= 10.0)) {
                AP->Vmax = Vm;
            }
            else AP->Vmax_switch = 1;
            if (AP->Vmax_switch == 1) {
                AP->APD_switch = 2;
                AP->AMP_last = AP->AMP;
                AP->AMP = AP->Vmax - AP->Vmin;
                AP->v90 = AP->Vmax - 0.9 * (AP->Vmax - AP->Vmin);
                AP->v75 = AP->Vmax - 0.75 * (AP->Vmax - AP->Vmin);
                AP->v50 = AP->Vmax - 0.50 * (AP->Vmax - AP->Vmin);
                AP->v30 = AP->Vmax - 0.30 * (AP->Vmax - AP->Vmin);
                // printf("asfde\n");
            }
        }
    }
    if (AP->APD_switch == 2) {

        if ((AP->Vm_prev >= AP->v30) && (Vm <= AP->v30) ) {
            AP->APD30 = t - AP->timeAPDstart ;
            // printf("aaaa\n");

        }
        else if ((AP->Vm_prev >= AP->v50) && (Vm <= AP->v50 )) {
            AP->APD50 = t - AP->timeAPDstart ;
            // printf("bbbbbbbbb\n");

        }
        else if (AP->Vm_prev >= AP->v75 && Vm <= AP->v75 ) {
            AP->APD75 = t - AP->timeAPDstart ;
            // printf("ccccccccccc\n");

        }
        else if (AP->Vm_prev >= AP->v90 && Vm <= AP->v90) {
            AP->APD_switch = 0;
            AP->APD_count ++;
            AP->Vmax_switch = 0;
            AP->APD90_prev = AP->APD90;
            AP->APD90 = t - AP->timeAPDstart;
            // printf("dddddddddddd\n");
            fprintf (out, "%.2f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n", AP->APD_count * BCL, AP->APD90, AP->APD75, AP->APD50, AP->APD30, AP->Vmax, AP->Vmin, AP->dVdtmax);
            if (AP->APD_out_swhich == 0) {
                AP->APD_out_end[0] = t - AP->timeAPDstart;
                AP->APD_out_swhich = 1;
            }
            else if (AP->APD_out_swhich == 1) {
                AP->APD_out_end[1] = t - AP->timeAPDstart;
                AP->APD_out_swhich = 0;
            }
        }
    }

    double dVdt = (Vm - AP->Vm_prev) / dt;
    if (dVdt > AP->dVdtmax) AP->dVdtmax = dVdt;

    AP->Vm_prev = Vm;
}



void report_last(AP_infor* AP) {

printf("%d %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n", AP->APD_count, AP->APD90, AP->APD75, AP->APD50, AP->APD30, AP->Vmax, AP->Vmin, AP->dVdtmax);

}

/* measure the accumulations of calcium build up from the ICaL and RyR */
void calcium_accumulation(double t, double Istim, double BCL, double dt, double ICaL_con, double J_RyR, AP_infor *AP, FILE *out) {
    if ((Istim >= 1e-3 || Istim <= -1e-3 ) && fabs(AP->Istim_prev) < 1e-3) {
        // printf("%s\n", );
        fprintf(out, "%.3f %f %f\n", t, AP->ICaL_in, AP->RyR_in);
        AP->timeAPDstart = t;
        AP->ICaL_in = 0.0;
        AP->RyR_in = 0.0;
    }

    AP->ICaL_in += ICaL_con * dt;
    AP->RyR_in += J_RyR * dt;
    AP->Istim_prev = Istim;
}





#endif