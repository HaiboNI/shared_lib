/*
 * pace_map_creator.c
 * Jonathan D. Stott <jonathan.stott@gmail.com>
 *
 */

#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>
#define NX 325
#define NY 325
#define NZ 425
/*#define NX (3)
#define NY (102)
#define NZ (3)*/

/*
#define NX (75)
#define NY (125)
#define NZ (125)
*/

//#define NCELL 1604158

unsigned char tissue[NZ][NY][NX];
//unsigned char stim[NCELL];
//int map[NCELL][3];

int main (int argc, char **argv) {

    unsigned char *stim;
    int **map;

    gzFile gz;
    int i, j, k, c, n, idx, stims, radius, g, h;
    int p[3];
    float distance;
    char filename[128];
    FILE *logfile, *out;
    char *str;
    int count;


    // check for args
    if (argc < 5) {
        fprintf(stderr, "Usage: %s z y x radius\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    logfile = fopen("Run_log_for_git.txt", "w");
    fprintf(logfile, "%s %s", argv[1], argv[2]);
    fclose(logfile);

    // read in the geomety
    // gz = gzopen("EXTRACTED_SAN.geo.gz", "r");
    gz = gzopen("Repaired_PF.geo.gz", "r");
    gzread(gz, tissue, NZ * NY * NX);
    gzclose(gz);

    // get the index and the radius from the commandline
    //   idx = atoi(argv[1]);
    radius = atoi(argv[4]);
    radius = radius * radius;

    p[0] = atoi(argv[1]);
    p[1] = atoi(argv[2]);
    p[2] = atoi(argv[3]);

    count = 0;
    for (k = 0; k < NZ; k++)
        for (j = 0; j < NY; j++)
            for (i = 0; i < NX; i++)
                if (tissue[k][j][i] != 0) count++;

    printf("%d\n", count);

    map = malloc(count * sizeof(int *));
    for (i = 0; i < count; i++) map[i] = malloc(3 * sizeof(int));

    // loop over the tissue, assigning coords
    c = 0;
    //    count = 0;
    stims = 0;
    for (k = 0; k < NZ; k++)
        for (j = 0; j < NY; j++)
            for (i = 0; i < NX; i++)
                if (tissue[k][j][i] != 0) {
                    map[c][0] = k;
                    map[c][1] = j;
                    map[c][2] = i;

                    // if this is the point we want, save it
                    /*        if (c == idx) {
                                p[0] = k;
                                p[1] = j;
                                p[2] = i;
                            }*/


                    c++;

                }

    //  p[0] = 150;//173;
    //  p[1] = 150;//173;
    //  p[2] = 20;

    stim = malloc(c * sizeof(unsigned char));

    printf("%d\n", c);

    // loop over the coords, checking if we're close.
    for (n = 0; n < c; ++n) {
        stim[n] = 0;

        distance = 0.0;
        for (i = 0; i < 3; ++i)
            // radius
            distance += ((map[n][i] - p[i]) * (map[n][i] - p[i]));
            // distance += ((map[n][1] - p[1]) /** (map[n][i] - p[i])*/) / 3.0;

        // if we're within 10 cells also SAN section, COMMENT if ectopic focus.
        if (distance < radius) {
            stim[n] = 1;
            stims++;
        }
    }

    printf("stims: %d\n", stims);

    // write the file, gzipped
    sprintf(filename, "PF_Repaired_%03d%03d%03d-%02s.mat.gz", p[0], p[1], p[2], argv[4]);
    gz = gzopen(filename, "wb9");
    gzwrite(gz, stim, count);
    gzclose(gz);

    str = malloc (20 * sizeof(char));
    sprintf(str, "h%03d%03d%03d.vtk", p[0], p[1], p[2]);
    out = fopen (str, "wt");

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "ASCII\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
    fprintf (out, "SPACING 0.00033 0.00033 0.00033\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", NX * NY * NZ);
    fprintf (out, "SCALARS ImageFile float 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");

    n = 0;
    for (g = 0; g < NZ; g++) {
        for (h = 0; h < NY; h++) {
            for (i = 0; i < NX; i++) {
                //                offset = (g*NX*NY) + (h*NX) + i;
                if (tissue[g][h][i] == 0) {
                    fprintf (out, "%d ", -100);
                } else {
                    //              s2[n] = bin(s2[n]);
                    fprintf (out, "%d ", stim[n]);
                    n++;
                }
            }
            fprintf(out, "\n");
        }
        fprintf(out, "\n");
    }

    printf("%d\n", n);

    fclose(out);
    free(stim);
    for (i = 0; i < count; i++) free(map[i]);
    free(map);

    return 0;
} /* end of main() */

