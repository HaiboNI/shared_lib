/*
 * image_data_writer.cc
 * Jonathan D. Stott <jonathan.stott@gmail.com>
 * Converted to output binary by Timothy D. Butters
 *
 */
#include <stdio.h>
#include <zlib.h>
#include <endian.h>
#include <stdlib.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <string.h>
/*#define NX (3)
#define NY (102)
#define NZ (3)*/

// /* New Atria Geometry */
// #define NX (239)
// #define NY (271)
// #define NZ (300)
/* Downsampled IS ventricle dimension */
/*#define NX (459)
#define NY (325)
#define NZ (357)
*/

#define NX 462
#define NY 325
#define NZ 358



#define MIN (-80.0)
#define MAX (20.0)

unsigned char geo[NZ][NY][NX];
int out_voltage[NZ][NY][NX];



int main (int argc, char **argv) {
    int g, h, i, offset, n;
    char command[256];
    gzFile gz;
    int count;
    FILE *p, *out;

    // gz = gzopen("Last_Atria_Geo_Full.geo.gz", "r");
    gz = gzopen(argv[1], "r");
    gzread(gz, geo, NZ * NY * NX);
    gzclose(gz);
    int count2 = 0;
    /*LV only */
	for (g = 0; g < NZ; ++g) {
		for (h = 0; h < NY; ++h) {
			for (i = 0; i < NX; ++i) {
				if (geo[g][h][i] > 0) {
					unsigned char label = geo[g][h][i];
					if (label == 50)   // RV stim points, set to 1 RVENDO
						label = 1;
					if (label == 75)   // RV stim points, set to 20 LVEPI
						label = 20;
					if (label == 100)   // LV stim points, set to 11 LVENDO
						label = 11;
                    if (label >= 1 and label <= 10) {
                        
                        label = 0; // RV
                    }
					if (label >= 1 and label <= 10) {
						/*if (label >= 1 and label <= 2) {
							label = 1; // RVENDO
						} else if (label >= 3 and label <= 5) {
							label = 2; // RVM
						} else {
							label = 3;  // RVEPI
						}*/

					} else if (label >= 11 and label <= 20) {
						label = 2; // LV
                        count2 ++;
					} else {
						/*std::cout << "cell type " << label << "should dose not exist!" << std::endl;
						std::exit(0);*/
					}

					if (label > 6 || label < 0) {
						std::cerr << "tissue label error, at i,h,g = " << i << ' ' << h << ' ' << g  << " and tissue [n] = " << label << std::endl;
					}
					geo[g][h][i] = label;
				}
			}
		}
	}

    std::cout << "count = " << count2 << std::endl;

    gz = gzopen("LV_Only.geo.gz", "wb9");
    if (!gz) {
        printf("Geometry file not opened!!!++++?\n");
        exit(0);
    }
    gzwrite(gz, geo, NZ * NY * NX);
    gzclose(gz);
    std::string vtk_out;
    vtk_out = "RV_Only.geo.gz";
    vtk_out += ".vtk";
    out = fopen (vtk_out.c_str(), "wt");
    fprintf(out, "# vtk DataFile Version 3.0\n");
    fprintf(out, "vtk output\n");
    fprintf(out, "ASCII\n");
    fprintf(out, "DATASET STRUCTURED_POINTS\n");
    fprintf(out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
    fprintf(out, "SPACING 1 1 1\n");
    fprintf(out, "ORIGIN 0 0 0\n");
    fprintf(out, "POINT_DATA %d\n", NX * NY * NZ);
    fprintf(out, "SCALARS HumanVentricleSegmentation int 1\n");
    fprintf(out, "LOOKUP_TABLE default\n");

    int space = htobe32(0);
    int S;
    int gg, hh, ii, nn;
    n = 0;
    for (g = 0; g < NZ; ++g) {
        for (h = 0; h < NY; ++h) {
            for (i = 0; i < NX; ++i) {
                fprintf(out, "%d ", (int) (geo[g][h][i]));
            }
            fprintf(out, "\n");
        }
    }

    fclose(out);

    return 0;
} /* end of main() */
