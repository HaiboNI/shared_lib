/*****************************************************************

    File Name : Filament_trace.cpp (compile with g++ Filament_trace.cpp -o Filament_trace)

    Purpose : Trace the tip (core) of spiral waves in a 2D model

    Code based on phase singularity method

    Creation Date : 22-10-2015

    Last Modified : 22-10-2015

    Author : Dominic Whittaker

*****************************************************************/

#include <iostream>
#include <iomanip>
#include <istream>
#include <vector>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <assert.h>
#include <string>
#include <sstream>

using namespace std;

#define NX 251
#define NY 251
#define NZ 1
#define Vrefy -40 //reference values CANNOT be selected randomly
#define Vrefx -50 //need to be enclosed by orbits of all trajectories in state space
#define MAX_LENGTH 10
#define DELAY 5 //tau value


double phase[NX][NY][NZ];
double V0[NX][NY];
double V1[NX][NY];
double kx[NX][NY][NZ];
double ky[NX][NY][NZ];
double kz[NX][NY][NZ];
double az[NX][NY][NZ];
double ay[NX][NY][NZ];
double ax[NX][NY][NZ];
double curl[NX][NY][NZ];

double diff(int nx, int ny, int nz, int direction) {

	double difference;

	if (direction < 1 || direction > 3) printf("Bad direction\n");

	if (direction == 1) // x
		difference = phase[nx + 1][ny][nz] - phase[nx][ny][nz];
	if (direction == 2) // y
		difference = phase[nx][ny + 1][nz] - phase[nx][ny][nz];
	if (direction == 3) // z
		difference = phase[nx][ny][nz + 1] - phase[nx][ny][nz];

	return difference;

} // end of differential function

// http://medphysics.wisc.edu/~ethan/phaseunwrap/unwrap.c
void unwrap(float p[], int NTOT)
// ported from matlab (Dec 2002)
{
	float       dp[MAX_LENGTH];
	float      dps[MAX_LENGTH];
	float  dp_corr[MAX_LENGTH];
	float   cumsum[MAX_LENGTH];
	float cutoff = M_PI;               /* default value in matlab */
	int j;

	assert(NTOT <= MAX_LENGTH);

	// incremental phase variation
	// MATLAB: dp = diff(p, 1, 1);
	for (j = 0; j < NTOT - 1; j++)
		dp[j] = p[j + 1] - p[j];

	// equivalent phase variation in [-pi, pi]
	// MATLAB: dps = mod(dp+dp,2*pi) - pi;
	for (j = 0; j < NTOT - 1; j++)
		dps[j] = (dp[j] + M_PI) - floor((dp[j] + M_PI) / (2 * M_PI)) * (2 * M_PI) - M_PI;

	// preserve variation sign for +pi vs. -pi
	// MATLAB: dps(dps==pi & dp>0,:) = pi;
	for (j = 0; j < NTOT - 1; j++)
		if ((dps[j] == -M_PI) && (dp[j] > 0))
			dps[j] = M_PI;

	// incremental phase correction
	// MATLAB: dp_corr = dps - dp;
	for (j = 0; j < NTOT - 1; j++)
		dp_corr[j] = dps[j] - dp[j];

	// Ignore correction when incremental variation is smaller than cutoff
	// MATLAB: dp_corr(abs(dp)<cutoff,:) = 0;
	for (j = 0; j < NTOT - 1; j++)
		if (fabs(dp[j]) < cutoff)
			dp_corr[j] = 0;

	// Find cumulative sum of deltas
	// MATLAB: cumsum = cumsum(dp_corr, 1);
	cumsum[0] = dp_corr[0];
	for (j = 1; j < NTOT - 1; j++)
		cumsum[j] = cumsum[j - 1] + dp_corr[j];

	// Integrate corrections and add to P to produce smoothed phase values
	// MATLAB: p(2:m,:) = p(2:m,:) + cumsum(dp_corr,1);
	for (j = 1; j < NTOT; j++)
		p[j] += cumsum[j - 1];
}

int main(int argc, char **argv)  {


// 	#define VTKSTART 550 //this is when "re-entry" begins
// #define VTKEND 1999


	int VTKSTART = 0;
	int VTKEND = 10000;

	VTKSTART = atoi(argv[1]);
	VTKEND = atoi(argv[2]);

	// variable declarations
	char * str, * str2;
	FILE * out, * out2, * spiral, * core;
	fstream file, file_delay;
	stringstream ss, ss_delay;
	char filename[60], filename_delay[60], filename_spiral[60], filename_ncores[60];
	vector < vector <int> > array; // 2D array as a vector of vectors
	vector < vector <int> > array_delay; // 2D array as a vector of vectors
	vector <int> rowVector(NX); // vector to add into 'array' (represents a row)
	int tip_x, tip_y, c, i, j, k, row, ncores, count;
	const int HEADER = 10; // header on .vtk files is 10 lines long
	float p[2];

	sprintf(filename_spiral, "Spiral_WT.txt");
	spiral = fopen(filename_spiral, "a");

	sprintf(filename_ncores, "NCores_WT.txt");
	core = fopen(filename_ncores, "a");


	for (c = VTKSTART; c < VTKEND - DELAY; c++) {
		// for(c=VTKSTART;c<VTKEND;c++) {

		// read input .vtk files
		ss << "v" << std::setfill('0') << std::setw(4) << c << ".vtk";
		ss_delay << "v" << std::setfill('0') << std::setw(4) << c + DELAY << ".vtk";

		std::cout << ss.str().c_str() << std::endl;
		file.open(ss.str().c_str()); // open file corresponding to V(t)
		file_delay.open(ss_delay.str().c_str()); // open file corresponding to V(t+T)

		ss.str("");
		ss_delay.str("");

		if (file.is_open()) { // if file has correctly opened
			// output debug message
			// cout << "File correctly opened" << endl;

			row = 0;
			for (i = 0; i < HEADER; i++) {
				file.ignore(500, '\n'); // ignore 10 line header on .vtk files
			}

			// dynamically store data into array
			while (file.good()) { // while there are no errors

				array.push_back(rowVector); // add a new row
				for (int col = 0; col < NX; col++) {
					float a;
					file >> a;
					array[row][col] = int(a); // fill the row with col elements
				}
				row++; // keep track of actual row
			}
		}

		else cout << "Unable to open file" << endl;
		file.close();

		// read in values for V(t+T)
		if (file_delay.is_open()) {

			row = 0;
			for (i = 0; i < HEADER; i++) {
				file_delay.ignore(500, '\n');
			}

			while (file_delay.good()) {

				array_delay.push_back(rowVector);
				for (int col = 0; col < NX; col++) {
					float a;

					file_delay >> a;
					array_delay[row][col] = int(a);
				}
				row++;
			}
		}

		else cout << "Unable to open file" << endl;
		file_delay.close();

		// assign voltages to V(t) and V(t+T)
		for (i = 0; i < NX; i++)
			for (j = 0; j < NY; j++)
				for (k = 0; k < NZ; k++) {
					V1[i][j] = array_delay[i][j];
					V0[i][j] = array[i][j];
				}

		array.clear();
		array_delay.clear();
		if (!array.empty() || !array_delay.empty()) std::cout << "Error clearing vectors." << std::endl;

		// compute phase using four-quadrant inverse tangent, gives phi in the range [-pi,+pi]
		for (i = 0; i < NX; i++)
			for (j = 0; j < NY; j++)
				for (k = 0; k < NZ; k++) {
					phase[i][j][k] = atan2((V1[i][j] - Vrefy), (V0[i][j] - Vrefx));
				}

		for (i = 0; i < NX; i++)
			for (j = 0; j < NY; j++)
				for (k = 0; k < NZ; k++) {
					kx[i][j][k] = -10;
					ky[i][j][k] = -10;
				}

		// calculate components of wave vector k
		for (i = 1; i < NX - 1; i++)
			for (j = 1; j < NY - 1; j++)
				for (k = 0; k < NZ; k++) {
					kx[i][j][k] = diff(i, j, k, 1);
					ky[i][j][k] = diff(i, j, k, 2);

					// unwrapping prevents discontinuities in phase
					p[0] = p[1] = 0.0;
					p[0] = 0.0; p[1] = kx[i][j][k];
					unwrap(p, 2);
					kx[i][j][k] = p[1];
					p[0] = p[1] = 0.0;
					p[0] = 0.0; p[1] = ky[i][j][k];
					unwrap(p, 2);
					ky[i][j][k] = p[1];

				}

		for (i = 0; i < NX; i++)
			for (j = 0; j < NY; j++)
				for (k = 0; k < NZ; k++) {
					az[i][j][k] = 0.0;
				}

		// only need to evaluate curl of k in z direction as using 2D sheet in xy plane
		for (i = 1; i < NX - 2; i++)
			for (j = 1; j < NY - 2; j++)
				for (k = 0; k < NZ; k++) {
					az[i][j][k] = ky[i + 1][j][k] - ky[i][j][k] - (kx[i][j + 1][k] - kx[i][j][k]);
					curl[i][j][k] = az[i][j][k] / (M_PI); // divide by pi to obtain an integer
				}

		// record tip coordinates for trajectory visualisation
		count = 0;
		for (i = 0; i < NX; i++)
			for (j = 0; j < NY; j++)
				for (k = 0; k < NZ; k++) {
					if ((int)curl[i][j][k] != 0) {
						count++;
						tip_x = j;
						tip_y = i;
						// output tip trajectory
						fprintf(spiral, "%d\t %d\t %d\n", tip_x, tip_y, c);
						ncores = count;
						// printf("ncores = %d\ttime = %d\n", ncores, c);
					}
				}
		// output #cores (should always be 1 unless multiple wavelets are present, or the choice of tau is bad)
		fprintf(core, "%d\t %d\n", c, ncores);

		// this outputs phase map to clearly visualise phase singularity
		str = (char *)malloc (30 * sizeof(char));
		sprintf(str, "phase_%04d.vtk", c);
		out = fopen (str, "wt");
		free(str);
		fprintf (out, "# vtk DataFile Version 3.0\n");
		fprintf (out, "vtk output\n");
		fprintf (out, "ASCII\n");
		fprintf (out, "DATASET STRUCTURED_POINTS\n");
		fprintf (out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
		fprintf (out, "SPACING 0.00025 0.00025 0.00025\n");
		fprintf (out, "ORIGIN 0 0 0\n");
		fprintf (out, "POINT_DATA %d\n", NX * NY * NZ);
		fprintf (out, "SCALARS PHASE double 1\n");
		fprintf (out, "LOOKUP_TABLE default\n");

		for (i = 0; i < NX; i++) {
			for (j = 0; j < NY; j++) {
				for (k = 0; k < NZ; k++) {
					fprintf(out, "%f ", phase[i][j][k]);
				}
			}
			fprintf(out, "\n");
		}
		fclose(out);

		// this outputs tip (non-zero curl) at each time step (in Paraview, enable opacity function and overlay on phase map in order to check that tip corresponds to phase singularity)
		str2 = (char *)malloc (30 * sizeof(char));
		sprintf(str2, "curl_%04d.vtk", c);
		out2 = fopen (str2, "wt");
		free(str2);
		fprintf (out2, "# vtk DataFile Version 3.0\n");
		fprintf (out2, "vtk output\n");
		fprintf (out2, "ASCII\n");
		fprintf (out2, "DATASET STRUCTURED_POINTS\n");
		fprintf (out2, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
		fprintf (out2, "SPACING 0.00025 0.00025 0.00025\n");
		fprintf (out2, "ORIGIN 0 0 0\n");
		fprintf (out2, "POINT_DATA %d\n", NX * NY * NZ);
		fprintf (out2, "SCALARS CURL int 1\n");
		fprintf (out2, "LOOKUP_TABLE default\n");

		for (i = 0; i < NX; i++) {
			for (j = 0; j < NY; j++) {
				for (k = 0; k < NZ; k++) {
					fprintf(out2, "%d ", (int)curl[i][j][k]);
				}
			}
			fprintf(out2, "\n");
		}
		fclose(out2);


	} // end of loop over files

	return 0;
}