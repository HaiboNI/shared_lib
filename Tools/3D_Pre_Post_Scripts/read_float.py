'''
use as 
python read_float.py 0 1500 
will output data from the binary file, ranging from 0 to 1500, to file output.dat

Haibo.ni <haibo.ni0822@gmail.com>
'''


import numpy as np
import sys



def get_value_from_bin(filename):
        return np.fromfile(filename, dtype=np.float32,sep='')


def get_filename(file_pre, index):
        return file_pre+"%04d.bin" % index


timestart =int(sys.argv[1])
timeends =int(sys.argv[2])

file_pre = 'v'
outfile = 'out.dat'
try:
	file_pre = sys.argv[3]
except:
	print file_pre

try:
	outfile = sys.argv[4]
except:
	print outfile

cellid = [343044, 370590, 384069, 1129449, 1427281, 1509239]
# cellid = [20*251+20, 20*251+251-20, (251-20)*251+20, (251-20+1)*251-20, 126*251+126]




num_cell = len(cellid)


data = []

for i in range(num_cell): data.append([])


print len(cellid)
print data

for i in range(timestart, timeends):
        filename = get_filename(file_pre, i);
        print filename, 'done!'
        try:
            datain = get_value_from_bin(filename)
            for j in range(num_cell):
                data[j].append(datain[cellid[j]])
            # print np.max(datain) # not needed.
            datain=None
        except:
        	print filename, 'not found!'


print len(data)
print len(data[0])
with open(outfile, 'w') as f:
        for i in range(timestart-timestart, timeends-timestart):
                f.write((str(i+timestart)) + ' ')
                for j in range(num_cell):
                        f.write((str(data[j][i])) + ' ')
                f.write(('\n'))
