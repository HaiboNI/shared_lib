//#include "vtkUniformGrid.h"
//#include "vtkXMLImageDataWriter.h"
//#include "vtkUnsignedCharArray.h"
//#include "vtkPointData.h"
#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>

/*#define NX (325)
#define NY (325)
#define NZ (425)*/
#define NX (3)
#define NY (102)
#define NZ (3)


//#define NCELL 1604158

#define MIN (-80.0)
#define MAX (20.0)

unsigned char c[NZ][NY][NX];
int ccc[NZ][NY][NX];
float *s;
float *si;
int *apdswitch;
int *time;
int *APD;
char *Cell_type;

float bin(float value);

int main (int argc, char **argv) {
    int g, h, i, counter, offset, n;
    char command[256];
    gzFile gz;
    FILE *p, *out;// *outtext *in;
    char *str;
    int CVzup;
    int CVzdown;
    int first;
    int CVyplus;
    int CVYmins;
    int firstx, firsty, firstz;
    int peak, RP;
    unsigned int count;
    int early_san, early_RA, early_LA, late_san, late_RA, late_LA, earlyPV, latePV, earlyBB, lateBB, earlyAS, lateAS;

    int start_time, end_time;

    start_time = atoi(argv[1]);
    end_time = atoi(argv[2]);

    early_san = early_RA = early_LA = earlyPV = earlyBB = earlyAS = 3000;
    late_san = late_RA = late_LA  = latePV = lateBB = lateAS = 0;

    gz = gzopen("ONE_D_3X3X100.geo.gz", "r");
    gzread(gz, c, NZ * NY * NX);
    gzclose(gz);
    printf("1111asdfe\n");

    count = 0;
    for (g = 0; g < NZ; g++) {
        for (h = 0; h < NY; h++) {
            for (i = 0; i < NX; i++) {
                if (c[g][h][i] != 0) {
                    ccc[g][h][i] = 1;
                    count++;
                }
                else ccc[g][h][i] = 0;
            }
        }
    }
    const unsigned long int CELL_NUM = count;
    printf("CELL_NUM = %d\n", count);
    Cell_type = (char *) malloc(CELL_NUM * sizeof(char));
    count = 0;

    int nn = 0;
    for (g = 0; g < NZ; g++) {
        for (h = 0; h < NY; h++) {
            for (i = 0; i < NX; i++) {
                if (c[g][h][i] != 0) {
                    Cell_type[nn] = c[g][h][i];
                    nn++;
                }

            }
        }
    }


    s = malloc(CELL_NUM * sizeof(float));
    si = malloc(CELL_NUM * sizeof(float));

    time = malloc(CELL_NUM * sizeof(int));
    apdswitch = malloc(CELL_NUM * sizeof(int));
    APD = malloc(CELL_NUM * sizeof(int));





    for (n = 0; n < CELL_NUM; n++) {
        time[n] = 5000;
        apdswitch[n] = 1;
        APD[n] = 5000;
    }
    printf("count\n");


    for (counter = start_time; counter < end_time; counter++) {

        // printf("counter = %d\n", counter);

        str = malloc (25 * sizeof(char));
        sprintf(str, "v%04d.bin.xz", counter);


        sprintf(command, "xzcat %s", str);
        p = popen(command, "r");
        fread(s, sizeof(float), CELL_NUM, p);
        pclose(p);
        free(str);

        n = 0;

        for (n = 0; n < CELL_NUM; ++n)
        {
            if (time[n] == 5000) {
                if (s[n] > -20) {
                    time[n] = counter;
                }
            }
            else {
                if (apdswitch[n] == 1) {
                    if (Cell_type[n] == 101) RP = -60;
                    else RP = -80;
                    if (s[n] < -60/*RP + 5*/) {
                        apdswitch[n] = 0;
                        APD[n] = counter - time[n];
                    }
                }
            }
        }
    } // end for


    first = 2500;
    n = 0;

    //find earliest activation
    for (g = 0; g < NZ; ++g) {
        for (h = 0; h < NY; ++h) {
            for (i = 0; i < NX; ++i) {

                if ((c[g][h][i] != 0))
                    if (time[n] < 5000) {
                        if (time[n] < first) {
                            first = time[n];
                            firstx = i;
                            firsty = h;
                            firstz = g;
                        }
                        n++;
                    }
            }
        }
    }


    printf("first = %d\n", first);

    printf("firstx = %d\n", firstx);
    printf("firsty = %d\n", firsty);
    printf("firstz = %d\n", firstz);

    printf("first activation: %d\n First SAN: %d\n  Last SAN: %d\n  First RA: %d\n Last RA %d\n First LA %d\n Last LA %d\n", first, early_san, late_san, early_RA, late_RA, early_LA, late_LA);
    printf("First PV: %d\n  Last PV: %d\n  First BB: %d\n Last BB %d\n First AS %d\n Last AS %d\n", earlyPV, latePV, earlyBB, lateBB, earlyAS, lateAS);


    //  str = malloc (8*sizeof(char));
    //   sprintf(str, "A%s.vtk", i);

    out = fopen ("APD_distribution.vtk", "wt");

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "ASCII\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
    fprintf (out, "SPACING 0.33 0.33 0.33\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", NX * NY * NZ);
    fprintf (out, "SCALARS ImageFile float 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");

    n = 0;
    for (g = 0; g < NZ; ++g) {
        for (h = 0; h < NY; ++h) {
            for (i = 0; i < NX; ++i) {
                offset = (g * NX * NY) + (h * NX) + i;
                if (c[g][h][i] == 0) {
                    fprintf (out, "%d ", -100);
                } else {
                    fprintf (out, "%d ", APD[n]);
                    n++;
                }
            }
            fprintf(out, "\n");
        }
        fprintf(out, "\n");
    }

    out = fopen ("Activation.vtk", "wt");

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "ASCII\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
    fprintf (out, "SPACING 0.33 0.33 0.33\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", NX * NY * NZ);
    fprintf (out, "SCALARS ImageFile float 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");

    n = 0;
    for (g = 0; g < NZ; ++g) {
        for (h = 0; h < NY; ++h) {
            for (i = 0; i < NX; ++i) {
                offset = (g * NX * NY) + (h * NX) + i;
                if (c[g][h][i] == 0) {
                    fprintf (out, "%d ", -100);
                } else {
                    fprintf (out, "%d ", time[n]);
                    n++;
                }
            }
            fprintf(out, "\n");
        }
        fprintf(out, "\n");
    }
    //free(str);
    fclose(out);

    out = fopen ("Repolarisaton.vtk", "wt");

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "ASCII\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
    fprintf (out, "SPACING 0.33 0.33 0.33\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", NX * NY * NZ);
    fprintf (out, "SCALARS ImageFile float 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");

    n = 0;
    for (g = 0; g < NZ; ++g) {
        for (h = 0; h < NY; ++h) {
            for (i = 0; i < NX; ++i) {
                offset = (g * NX * NY) + (h * NX) + i;
                if (c[g][h][i] == 0) {
                    fprintf (out, "%d ", -100);
                } else {
                    fprintf (out, "%d ", time[n] + APD[n]);
                    n++;
                }
            }
            fprintf(out, "\n");
        }
        fprintf(out, "\n");
    }
    free(s);
    free(si);
    free(time);
    return 0;

} /* end of main() */

float bin(float value) {
    if (value >= MAX) {
        return 254;
    } else if (value <= MIN) {
        return 0;
    } else {
        return (unsigned char) (((value - MIN) / (MAX - MIN)) * 254);
    }
}
