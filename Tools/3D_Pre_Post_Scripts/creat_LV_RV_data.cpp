/*
 * image_data_writer.cc
 * Jonathan D. Stott <jonathan.stott@gmail.com>
 * Converted to output binary by Timothy D. Butters
 * Updated by Haibo Ni <haibo.ni.0822@gmail.com>
 *
 */
#include <stdio.h>
#include <zlib.h>
#include <endian.h>
#include <stdlib.h>
#include <assert.h>
#include <cstdlib>
#include <iostream>
#include <string>
/*#define NX (3)
#define NY (102)
#define NZ (3)*/

// /* New Atria Geometry */
// #define NX (239)
// #define NY (271)
// #define NZ (300)
/* Downsampled IS ventricle dimension */
/*#define NX (459)
#define NY (325)
#define NZ (357)
*/

#define NX (462)
#define NY (325)
#define NZ (358)


#define MIN (-80.0)
#define MAX (20.0)

unsigned char geo[NZ][NY][NX];
float *s;
float out_voltage[NZ][NY][NX];
float ReverseFloat(const float inFloat)
{
	// reverse endian of float 32;
	// from Erick
	// from link:
	// http://stackoverflow.com/questions/2782725/converting-float-values-from-big-endian-to-little-endian
	float retVal;
	char *floatToConvert = ( char *) & inFloat;
	char *returnFloat = ( char *) & retVal;
	// swap the bytes into a temporary buffer
	returnFloat[0] = floatToConvert[3];
	returnFloat[1] = floatToConvert[2];
	returnFloat[2] = floatToConvert[1];
	returnFloat[3] = floatToConvert[0];
	return retVal;
}



int main (int argc, char **argv) {
	int g, h, i, offset, n;
	char command[256];
	gzFile gz;
	int count;
	FILE *p, *out;

	if (argc != 2)  {
		std::cerr << " ------------ input argument number must be 1! -------------" << std::endl;
		std::cerr << " please run the code as ./vtk input.bin" << std::endl << std::endl;
		std::exit(0);
	}

	// gz = gzopen("Last_Atria_Geo_Full.geo.gz", "r");
	// gz = gzopen("Vent_seeman13.mat.gz", "r");
	gz = gzopen("Vent_seeman_Add_RV_Stim_RG.mat.gz", "r");
	if (!gz) {
		printf("Geometry file not opened!!!\n");
		exit(0);
	}

	gzread(gz, geo, NZ * NY * NX);
	gzclose(gz);

	int LVcount = 0;
	int RVcount = 0;
	for ( g = 0; g < NZ; g++) {
		for ( h = 0; h < NY; h++) {
			for ( i = 0 ; i < NX; i++) {
				unsigned char label = geo[g][h][i];
				if (label == 50)   // RV stim points, set to 1 RVENDO
					label = 1;
				if (label == 75)   // RV stim points, set to 20 LVEPI
					label = 20;
				if (label == 100)   // LV stim points, set to 11 LVENDO
					label = 11;
				if (label != 0 ) {
					if (label >= 1 and label <= 10)
					{
						RVcount ++;
						label = 1;
					} else if (label >= 11 and label <= 20)
					{
						LVcount ++;
						label = 2;
					}
				}
				geo[g][h][i] = label;
			}
		}
	}

	printf(" total num of RV cells: %d\n", RVcount);
	printf(" total num of LV cells: %d\n", LVcount);


	s = new float [LVcount + RVcount];
	float *LV_data = new float [LVcount];
	float *RV_data = new float [RVcount];
	FILE *in;
	in = fopen(argv[1], "rb");
	fread(s, sizeof(float), (LVcount + RVcount), in);
	fclose(in);

	float max = -80.0;
	float min = 0.0;
	float space = ReverseFloat(-100);
	float S;
	int gg, hh, ii, nn;
	n = 0;
	gg = hh = ii = nn = -1;
	int LV_in, RV_in;
	LV_in = RV_in = 0;

	for (g = 0; g < NZ; ++g) {
		for (h = 0; h < NY; ++h) {
			for (i = 0; i < NX; ++i) {
				if (geo[g][h][i] == 0) {
					// fwrite (&space, sizeof(int), 1, out);
					out_voltage[g][h][i] = space;
				} else {
					// assert(s[n] == s[n]);  // failure would indicate NaNs

					if (geo[g][h][i] == 1)
					{
						RV_data[RV_in] = s[n];
						// printf("%f %f %d\n", s[n], RV_data[RV_in], n);
						RV_in ++;
					} else if (geo[g][h][i] == 2)
					{
						LV_data[LV_in] = s[n];
						LV_in ++;
					} else {
						printf("Geometry label wrong after changing to LV/RV\n");
						exit(0);
					}
					n ++;
				}
			}
		}
	}
	std::string str, str1;
	std::string str0(argv[1]);
	str = "RV." + str0;
	out = fopen (str.c_str(), "wb");
	fwrite(RV_data, sizeof(float), RVcount, out);
	fclose(out);
	str1 = "LV." + str0;
	out = fopen (str1.c_str(), "wb");
	fwrite(LV_data, sizeof(float), LVcount, out);

	
	fclose(out);
	delete [] s, LV_data, RV_data;
	return 0;
} /* end of main() */
