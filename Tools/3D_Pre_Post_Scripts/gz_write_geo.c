#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>
#include <math.h>

#define NX 13
#define NY 102
#define NZ 13

void write_float_binary_vtk(char *str, double ***data, int nx, int ny, int nz);
void write_float_binary_vtk(char *str, double ***data, const int nx, const int ny, const int nz) ;


// void write_binary_vtk(char *str, double ***data, int NX, int NY, int NZ);


int geometry_int[NZ][NY][NX];

unsigned char phi[NZ][NY][NX];
unsigned char theta[NZ][NY][NX];


unsigned char geometry[NZ][NY][NX];

unsigned char stim[NZ][NY][NX];

int main() {

    int i, j, k;
    int g, h, n, offset, e1;

    FILE *in, *out;
    gzFile gz;
    char *str;

    str = malloc (80 * sizeof(char));

    double FCell;


    /*gz = gzopen("ONE_D_3X3X100.geo.gz", "r");
    gzread(gz, c, NZ * NY * NX);
    gzclose(gz);
    */
    // printf("e0\n");

    for (k = 0; k < NZ; k++) {
        for (j = 0; j < NY; j++) {
            for (i = 0; i < NX; i++) {
                phi[k][j][i] = 0;
                theta[k][j][i] = 0;
                geometry[k][j][i] = 0;
                stim[k][j][i] = 0;
            }
        }
    }




    for (k = 1; k < NZ - 1; k++) {
        for (j = 1; j < NY - 1; j++) {
            for (i = 1; i < NX - 1; i++) {
                phi[k][j][i] = 255;
                theta[k][j][i] = 255;
                geometry[k][j][i] = 14;
                /*                if (k == 1 && i == 1)
                                {
                                    geometry[k][j][i] = 14;
                                }*/
            }
        }

    }



    for (k = 1; k < NZ - 1; k++) {
        for (j = 1; j < NY - 1; j++) {
            for (i = 1; i < NX - 1; i++) {

                if (j <= 10 && geometry[k][j][i] > 0)
                {
                    stim[k][j][i] = 1;
                }
                else {
                    stim[k][j][i] = 0;
                }
            }
        }

    }



    /*  in = fopen("Fcell.txt", "r");
        out = fopen("1DFcell.txt", "wt");
        for (k=0;k<NZ;k++){
            for (j=0;j<NY;j++){
                    for (i=0;i<NX;i++){
                        fscanf(in, "%d ", FCell);
                    if(geometry_int[k][j][i] != 0){
                        fprintf(out, "%d\n", FCell);
                    }
                }
            }
        }
        fclose(in);
        fclose(out);
    */
    gz = gzopen("ONE_D_13X102X13.geo.gz", "wb9");
    gzwrite(gz, geometry, NX * NY * NZ);
    gzclose(gz);

    gz = gzopen("ONE_D_13X102X13.phi.gz", "wb9");
    gzwrite(gz, phi, NX * NY * NZ);
    gzclose(gz);

    gz = gzopen("ONE_D_13X102X13.theta.gz", "wb9");
    gzwrite(gz, theta, NX * NY * NZ);
    gzclose(gz);


    gz = gzopen("ONE_D_13X102X13.stim.gz", "wb9");
    gzwrite(gz, stim, NX * NY * NZ);
    gzclose(gz);




    printf("e\n");

    out = fopen ("ONE_D_13X102X13.vtk", "wt");

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "BINARY\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
    fprintf (out, "SPACING 0.33 0.33 0.33\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", NX * NY * NZ);
    fprintf (out, "SCALARS HumanAtrium int 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");


    int space = htobe32(-100);
    int S;
    n = 0;
    for (g = 0; g < NZ; ++g) {
        for (h = 0; h < NY; ++h) {
            for (i = 0; i < NX; ++i) {
                if (geometry[g][h][i] != 0) {

                    // S = htobe32(geometry[g][h][i]);
                    S = htobe32(stim[g][h][i]);

                    fwrite(&S, sizeof(int), 1, out);
                    n++;

                } else {

                    fwrite (&space, sizeof(int), 1, out);

                }
            }
        }
    }


    fclose(out);

    unsigned char *** stim_out = stim;

    write_int_binary_vtk("test.vtk", stim, NX, NY, NZ);
}



void write_float_binary_vtk(char *str, double ***data, const int nx, const int ny, const int nz) {

    FILE *out;
    char filename[128];
    sprintf(filename, "%s", str);
    printf("%s\n", filename);
    out = fopen (filename, "wt");

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "BINARY\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", nx, ny, nz);
    fprintf (out, "SPACING 0.33 0.33 0.33\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", nx * ny * nz);
    fprintf (out, "SCALARS HumanAtrium int 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");
    int g, h, i;
    printf("%d %d \n", g, h);
    printf("%s\n", filename);

    float S;
    for (g = 0; g < nz; ++g) {
        for (h = 0; h < ny; ++h) {
            for (i = 0; i < nx; ++i) {
                printf("%d %d \n", g, h);
                // S = htobe32(geometry[g][h][i]);
                S = (float)(data[g][h][i]);
                fwrite(&S, sizeof(float), 1, out);
            }
        }
    }
    fclose(out);
}


void write_int_binary_vtk(char *str, unsigned char ***data, int nx, int ny, int nz) {

    FILE *out;
    char filename[128];

    sprintf(filename, "%s", str);
    printf("%s\n", filename);

    out = fopen (filename, "wt");
        if (out == 0) {
        fprintf(stderr, "failed to open file '%s' \n",filename);
        exit(EXIT_FAILURE);
    }

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "BINARY\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", nx, ny, nz);
    fprintf (out, "SPACING 0.33 0.33 0.33\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", nx * ny * nz);
    fprintf (out, "SCALARS HumanAtrium int 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");
    int g, h, i;
    printf("%d %d \n", g, h);
    printf("%s\n", filename);
    unsigned char S;
    int rw;
    for (g = 0; g < nz; ++g) {
        for (h = 0; h < ny; ++h) {
            // int * a = data[g][h];
                printf("%d %d \n", g, h);

            for (i = 0; i < nx; ++i) {
                S = data[g][h][i];
            fwrite(&S, sizeof(unsigned char), 1, out);
            }

        }
    }

    if (rw != nx * ny * nz) {
        fprintf(stderr, "%d/%d floats written to '%s', exiting!\n", rw,  nx * ny * nz, filename);
        exit(EXIT_FAILURE);
    }

    fclose(out);
}