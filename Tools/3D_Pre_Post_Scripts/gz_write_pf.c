#include <stdio.h>
#include <zlib.h>
#include <stdlib.h>
#include <math.h>

#define NX 325
#define NY 325
#define NZ 425

int geometry_int[NZ][NY][NX];
unsigned char c[NZ][NY][NX];


unsigned char geometry[NZ][NY][NX];


int main() {

    int i, j, k;
    int g, h, n, offset, e1;
FILE *fp;
    FILE *in, *out;
    gzFile gz;
    char *str;

    str = malloc (80 * sizeof(char));

    double FCell;


    // gz = gzopen("wholeHeartPF.geo.gz", "r");
    // gzread(gz, c, NZ * NY * NX);
    // gzclose(gz);



    in = fopen("PF_repaired_region_grow.bin", "rb");
 int len = fread(&c, sizeof(unsigned char), NX*NY*NZ, in);

    if (len != NZ*NY*NX)
    {
        fprintf(stderr, "data size not equal!\n" );
        printf("len = %d, \n", len);
    }

    printf("e0\n");

    for (k = 0; k < NZ; k++) {
        for (j = 0; j < NY; j++) {
            for (i = 0; i < NX; i++) {

                if (c[k][j][i] <= 15 && c[k][j][i] >= 13)

                {
                    // printf(" %d \n", c[k][j][i]);
                    geometry[k][j][i] = c[k][j][i];
                }
                else
                {
                    geometry[k][j][i] = 0;

                }
            }
        }

    }

	/*  in = fopen("Fcell.txt", "r");
        out = fopen("1DFcell.txt", "wt");
        for (k=0;k<NZ;k++){
            for (j=0;j<NY;j++){
                    for (i=0;i<NX;i++){
                        fscanf(in, "%d ", FCell);
                    if(geometry_int[k][j][i] != 0){
                        fprintf(out, "%d\n", FCell);
                    }
                }
            }
        }
        fclose(in);
        fclose(out);
    */
    gz = gzopen("Repaired_PF.geo.gz", "wb9");
    gzwrite(gz, geometry, NX * NY * NZ);
    gzclose(gz);

    printf("e\n");

    out = fopen ("new_region_growing_c_out.vtk", "wt");

    fprintf (out, "# vtk DataFile Version 3.0\n");
    fprintf (out, "vtk output\n");
    fprintf (out, "BINARY\n");
    fprintf (out, "DATASET STRUCTURED_POINTS\n");
    fprintf (out, "DIMENSIONS %d %d %d\n", NX, NY, NZ);
    fprintf (out, "SPACING 0.33 0.33 0.33\n");
    fprintf (out, "ORIGIN 0 0 0\n");
    fprintf (out, "POINT_DATA %d\n", NX * NY * NZ);
    fprintf (out, "SCALARS HumanAtrium int 1\n");
    fprintf (out, "LOOKUP_TABLE default\n");

    printf("e2\n");

    int space = htobe32(-1);
    int S;
    n = 0;
    for (g = 0; g < NZ; ++g) {
        for (h = 0; h < NY; ++h) {
            for (i = 0; i < NX; ++i) {
                if (geometry[g][h][i] != 0) {

                    S = htobe32(geometry[g][h][i]);
                    fwrite(&S, sizeof(int), 1, out);
                    n++;

                } else {

                    fwrite (&space, sizeof(int), 1, out);

                }
            }
        }
    }



    fclose(out);



}

