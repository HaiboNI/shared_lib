#!/bin/bash

icpc vtkwriter_bin.cpp -o vtk -lz

cp vtk ATRIUM_SAN.geo.gz make_video.py video_reentry_Atria.pvsm $1

cd $1

~/Paraview/bin/pvbatch make_video.py 10000 20000 10

cd -