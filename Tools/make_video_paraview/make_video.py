import sys
from paraview.simple import *

import sys, subprocess
import os
from subprocess import call

servermanager.LoadState("video_reentry_Atria.pvsm")
SetActiveView(GetRenderView())

# SetActiveView(GetViewsInLayout())
for viewid in GetRenderViews():
	viewid.UseOffscreenRenderingForScreenshots = 1
# Render()
# FindSource("AT.vtk").FileNames ='AT.vtk'

start = int(sys.argv[1])
end = int(sys.argv[2])
interval = int(sys.argv[3])
for i in range(start, end, interval):
	print FindSource("v10000.vtk")

	if (os.path.isfile('v%04d.bin' % (i))):

		subprocess.call(['./vtk', 'v%04d.bin' % (i), 'v%04d' %(i)])
		FindSource("v10000.vtk").FileNames ='v%04d.vtk' % (i)
		SaveScreenshot('v.%04d.png' % (i), layout=GetLayout(view=GetRenderView()))
		print "done", '%04d.png' % (i)
		if i>start+interval:
			subprocess.call(['rm', 'v%04d.vtk' %(i-interval)])
	else:
		print 'file ', 'v%04d.bin' % (i), ' not found!'
		break;
subprocess.call(['rm', '*.vtk'])