import pylab as p
import numpy as np

import matplotlib
from matplotlib.font_manager import FontProperties

matplotlib.rcdefaults()

# Set size of axes etc

#matplotlib.rc('lines', markeredgewidth=0.5)
matplotlib.rc('lines', markersize=7)
matplotlib.rc('xtick.major', size=5)
matplotlib.rc('ytick.major', size=5)
##matplotlib.rc('ytick.major', pad=6)
matplotlib.rc('xtick', direction='out')
matplotlib.rc('ytick', direction='out')

# Set font family and size
font = {'family' : 'monospace',
        'weight' : 'normal',
        'size'   : 8}

matplotlib.rc('font', **font)



#======================
# Open and process file
#======================

# EPI
f = np.loadtxt( 'myfile.txt', unpack=True)
# Load in the columns of text
epi_time 	= f[0]
epi_volt	= f[1]
epi_cai = f[2]

fig = p.figure(1)

#font = {'fontname':'Arial','fontsize':16}


##############
# AP
##############
# Create a subplot

# 2 subplots, first row, first column
ax = fig.add_subplot(211) 

# Plotvariable
ax.plot( epi_time, epi_volt, label='AP', linewidth=2)

# y-axis label
ax.set_ylabel('Potential (mV)', fontsize=12)

# Legend 
leg = p.legend(shadow='true', loc='best')
leg.draw_frame(False)

#p.axis([0.0, 0.002, 72.0, 80.0])

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.xaxis.set_ticks_position('none')
ax.yaxis.set_ticks_position('left')
ax.spines['bottom'].set_linewidth(2)
ax.spines['left'].set_linewidth(2)

# Set fontsize of y-axis ticks
for tick in ax.yaxis.get_major_ticks(): 
	tick.label1.set_fontsize(12)

# How many numbers per y-axis	
ax.yaxis.set_major_locator(p.MaxNLocator(5))

# Set fontsize of x-axis ticks
for tick in ax.xaxis.get_major_ticks(): 
	tick.label1.set_fontsize(12)

# How many numbers per x-axis
ax.xaxis.set_major_locator(p.MaxNLocator(5))

# Set width of x-ticks
xticks = p.getp(p.gca(), 'xticklines')
p.setp(xticks, markeredgewidth=2)

# Set width of x-ticks
yticks = p.getp(p.gca(), 'yticklines')
p.setp(yticks, markeredgewidth=2)

# Make axis labels invisible
[label.set_visible(False) for label in ax.get_xticklabels()]



##############
# Cai
##############

# 2 plots, 1st row, 2 column
ax = fig.add_subplot(212)

# Plotvariable
# We can use Latex in the labels etc
ax.plot( epi_time, epi_cai, label=r'$[Ca^{2+}]_{i}$', linewidth=2)

# y-axis label
ax.set_ylabel('mM', fontsize=12)

# Legend 
leg = p.legend(shadow='true', loc='best')
leg.draw_frame(False)

#p.axis([0.0, 0.002, 72.0, 80.0])

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.xaxis.set_ticks_position('none')
ax.yaxis.set_ticks_position('left')
ax.spines['bottom'].set_linewidth(2)
ax.spines['left'].set_linewidth(2)

# Set fontsize of y-axis ticks
for tick in ax.yaxis.get_major_ticks(): 
	tick.label1.set_fontsize(12)

# How many numbers per y-axis	
ax.yaxis.set_major_locator(p.MaxNLocator(5))

# Set fontsize of x-axis ticks
for tick in ax.xaxis.get_major_ticks(): 
	tick.label1.set_fontsize(12)

# How many numbers per x-axis
ax.xaxis.set_major_locator(p.MaxNLocator(5))

# Set width of x-ticks
xticks = p.getp(p.gca(), 'xticklines')
p.setp(xticks, markeredgewidth=2)

# Set width of x-ticks
yticks = p.getp(p.gca(), 'yticklines')
p.setp(yticks, markeredgewidth=2)

# Make axis labels invisible
[label.set_visible(False) for label in ax.get_xticklabels()]



# Write plot to file
p.savefig('figure.png')


# Show plot
p.show()

