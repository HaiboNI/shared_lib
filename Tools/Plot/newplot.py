import scipy.optimize as optimize
import numpy as np
import collections
import matplotlib.pyplot as plt
import csv
from matplotlib import rc
rc('mathtext', default='regular')
# EPI
f = np.loadtxt( 'baseline_states.dat', unpack=True)
# Load in the columns of text
epi_time = f[0]
epi_volt = f[1]
epi_cai  = f[2]

# x,y = np.random.randn(2,100)
x = epi_time;
y = epi_volt;
plt.rcParams['xtick.major.pad']='8'
plt.rcParams['ytick.major.pad']='8'
fig = plt.figure(1)
ax1 = fig.add_subplot(221)
# ax1.set_xlim((0,5000))

ax1.plot(x, y, 'k-', label = 'AP', linewidth = 2)
ax2 = ax1.twinx()
ax2.plot(epi_time, 1e5*epi_cai, 'r-', linewidth = 2, label = '$[Ca^{2+}]_{i}$')
ax2.set_ylim(20, 50)

# legend = ax1.legend(loc='upper right', shadow=False)
# ax2.legend(loc=1)
# frame  = legend.get_frame()
# frame.set_facecolor('0.90')
ax1.set_xlim([13990, 15000])
# ask matplotlib for the plotted objects and their labels
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc='best')
# plt.legend('asdfe')
# ax1.xcorr(x, y, usevlines=True, maxlags=50, normed=True, lw=2)
ax1.grid(True)
# ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.spines['bottom'].set_visible(True)
# ax1.axhline(0, color='black', lw=2)

# ax2 = fig.add_subplot(222, sharex=ax1)
# ax2.acorr(x, usevlines=True, normed=True, maxlags=50, lw=2)
# ax2.grid(True)
# ax2.axhline(0, color='black', lw=2)

ax3 = fig.add_subplot(223)
# ax3.acorr(x, usevlines=True, normed=True, maxlags=50, lw=2)
# ax3.grid(True)
# ax3.axhline(0, color='black', lw=2)

xp = np.linspace(-100, 100, 300)
y = np.exp(xp);
plt.xlim([1,20])
plt.plot(xp, y, '-')
plt.xlabel("what")
plt.title('Easy as 1,2,3')   # subplot 211 title
# ax3.plot()
plt.show()


