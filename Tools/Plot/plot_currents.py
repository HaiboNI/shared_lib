import matplotlib
import matplotlib.gridspec as gridspec
from pylab import *
from mytools import *
import matplotlib.pyplot as plt

rc('mathtext',default='regular')
xpos = -0.4
ypos = 0.5
lblsize = 15
boldness = 500
ticklblsize = 15
lnwidth = 2.5

#PF = numpy.loadtxt('PF.dat')
#endo = numpy.loadtxt('LV_endo.dat')
#M = numpy.loadtxt('LV_M.dat')
#epi = numpy.loadtxt('LV_epi.dat')

fig = figure(figsize=(10,14))
gs1 = gridspec.GridSpec(10,4)
#gs1.update(left=0.05, right=0.48, wspace=0.05)
#gs1.update(wspace=0.05)


for i in range(10) :

    for j in range(4) :
        if j==0 :
            ax = plt.subplot(gs1[i,j])
            ax.plot(PF[:,0],PF[:,i+1], 'k', linewidth=lnwidth)
        elif j==1 :     
            ax = plt.subplot(gs1[i, j])
            ax.plot(endo[:,0],endo[:,i+1], 'k', linewidth=lnwidth)
        elif j==2 :    
            ax = plt.subplot(gs1[i, j]) 
            ax.plot(M[:,0],M[:,i+1], 'k', linewidth=lnwidth)
        elif j==3 :
            ax = plt.subplot(gs1[i, j])
            ax.plot(epi[:,0],epi[:,i+1], 'k', linewidth=lnwidth)
   
        ax.autoscale(enable=True, axis='y')  
        ax.spines['right'].set_visible(False)
        #ax.spines['left'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.xaxis.set_ticks_position('none')
        ax.spines['bottom'].set_linewidth(1)
        ax.spines['left'].set_linewidth(1)
        [label.set_visible(False) for label in ax.get_xticklabels()]
        
        if i == 0 : 
            if j == 0 : 
                ax.yaxis.set_ticks([-80, -40, 0, 40]) 
                ylabel('V\n(mV)', fontsize=lblsize,ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
                ax.arrow(9250, 30, 100, 0)
            ax.axis([8990, 9400, -86, 45])
        elif i == 1 : 
            if j== 0 : 
                ax.yaxis.set_ticks([-500, 0]) 
                ylabel('$I_{Na}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -500, 10])
        elif i == 2 : 
            if j == 0 : 
                ax.yaxis.set_ticks([-2.5, 0]) 
                ylabel('$I_{NaL}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -2.5, 0.1])
        elif i == 3 : 
            if j == 0 : 
                ax.yaxis.set_ticks([-10, 0])
                ylabel('$I_{CaL}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -11, 0.3])
        elif i == 4 : 
            if j == 0 : 
                ax.yaxis.set_ticks([-0.5, 0])
                ylabel('$I_{CaT}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -0.5, 0.02])
        elif i == 5 : 
            if j == 0 : 
                ax.yaxis.set_ticks([0, 10])
                ylabel('$I_{to}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -0.5, 10])
        elif i == 6 : 
            if j == 0 : 
                ax.yaxis.set_ticks([0, 0.7])
                ylabel('$I_{Kr}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -0.05, 0.8])
        elif i == 7 : 
            if j == 0 : 
                ax.yaxis.set_ticks([0, 0.3])
                ylabel('$I_{Ks}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -0.02, 0.3])
        elif i == 8 : 
            if j == 0 : 
                ax.yaxis.set_ticks([0, 3.5])
                ylabel('$I_{K1}$' + '\n(pA/pF)', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, -0.05, 3.5])

        elif i == 9 :
            if j == 0 : 
                ax.yaxis.set_ticks([0, 1e-3]) 
                ylabel(r'$[Ca]_i$' + '\n' +  r'$(\mu M)$', fontsize=lblsize, ha='center',weight=boldness)
                ax.yaxis.set_ticklabels(['0', '1'])
                ax.yaxis.set_label_coords(xpos,ypos)
                rc('ytick',labelsize=ticklblsize)
            ax.axis([8990, 9400, 0, 1.4e-3])

        ax.get_yaxis().tick_left()
        if j != 0 : 
            [label.set_visible(False) for label in ax.get_yticklabels()]
            ax.yaxis.set_ticks_position('none')
        
        ax.axvline(linewidth=lnwidth)
        if j==4 and i==9 : ax.arrow(9200, 9300, 1e-4, 1e-4)
show()

