
write_plotgraph()
{
        i=1
        for i in `seq 3 1 299` 
        do
	num=$(printf "%d" $i)
        gnuplot <<- EOF
        set term png large size 1200,1024
        set output "new$num.png"
        set style data lines
        set ylabel "$num"
	set xlabel "Time"
        set autoscale
        plot "grandi_sa.dat" using 2:$num title "1:$num" lw 2 lc 3
	EOF
       
       done 
}
write_plotgraph


# set term postscript enhanced 12 linewidth 4 color

