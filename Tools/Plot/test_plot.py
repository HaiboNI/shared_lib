import scipy.optimize as optimize
import numpy as np
import collections
import matplotlib.pyplot as plt
import csv

import matplotlib.pyplot as plt
import numpy as np

x,y = np.random.randn(2,100)
fig = plt.figure(1)
ax1 = fig.add_subplot(221)
ax1.xcorr(x, y, usevlines=True, maxlags=50, normed=True, lw=2)
ax1.grid(True)
ax1.axhline(0, color='black', lw=2)

ax2 = fig.add_subplot(222, sharex=ax1)
ax2.acorr(x, usevlines=True, normed=True, maxlags=50, lw=2)
ax2.grid(True)
ax2.axhline(0, color='black', lw=2)

ax3 = fig.add_subplot(223, sharex=ax1)
ax3.acorr(x, usevlines=True, normed=True, maxlags=50, lw=2)
ax3.grid(True)
ax3.axhline(0, color='black', lw=2)

xp = np.linspace(-100, 100, 300)
y = np.exp(xp);
plt.plot(xp, y, '-')
ax3.legend('legend')
# ax3.plot()
plt.show()

