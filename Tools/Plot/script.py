import scipy.optimize as optimize
import numpy as np
import collections
import matplotlib.pyplot as plt
import csv
from matplotlib import rc
#rc('mathtext', default='regular')
# EPI

import os.path

def in_range(target, range_a, range_b):
	if ((target <= range_a) and (target >=range_b)) or ((target <= range_b) and (target >=range_a)) :
		return True
	else :
		return False

# note 
# the cai seems to be mistakenly taken as the nai


write_file = open('alldata.dat', 'w')
for i in range(1, 1000):
	filename = 'Output_grandi/cellData' + str(i) + '.dat'
	if os.path.isfile(filename):
		f = np.loadtxt( filename, unpack=True)
		data_index   = f[0]
		RP           = f[44]
		dvdtmax      = f[45]
		maxPotential = f[46]
		apd90        = f[47]
		apd30        = f[48]
		maxcai       = f[49]
		mincai       = f[50]
		maxnai       = f[51]
		minnai       = f[52]
		maxki        = f[53]
		minki        = f[54]
		#please modify the ranges properly
		# all the conditions are here:
		if (in_range(RP, -90, -60) and in_range(dvdtmax, 100, 200) and in_range(maxPotential, 10, 50) \
			and in_range(apd90, 200, 400) and in_range(apd30, 30, 100) and in_range(maxcai, 8, 10) \
			and in_range(mincai, 8, 10) and in_range(maxnai, 0.000002, 0.001) and in_range(minnai, 0.000002, 0.001) \
			and in_range(maxki, 0.1, 1.5) and in_range(minki, 0.1, 1.5)):
			print data_index
			print '\n'
			write_file.write(str(int(f[0])))
			write_file.write(str('\n'))
		#load in the columns of text
	else:
		name = 'cellData' + str(i) + '.dat'
		print 'file: ' + name + ' does not exist!\n'


# print epi_cai

