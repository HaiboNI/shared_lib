/*read in a single txt file,
 * output as a binary file, to be further processed using vtk. ..

 * Haibo Ni Tue 05 May 2015 10:39:51 BST
 * haibo.ni0822@gmail.com
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define count 1600441

int main() {

	FILE *in, *out;
	float s_t;
	double FCell;
	float *data_in;
	data_in = (float*) malloc (count * sizeof(float));

	in = fopen("phase_map_3D.txt", "r");

	if(!in) {
		fprintf(stderr, "input txt file was not opened!!!\n");
	}

	int i = 0;
	while (!feof(in)) {
		fscanf(in, "%f\n", &data_in[i]);
		i++;
	}
	fclose(in);
	if(i != count) {
		fprintf(stderr, "i = %d, count = %d\n", i, count);
		exit(0);
	}

	out = fopen("phase_map_3D_bin.bin", "wb");

	fwrite(data_in, count, sizeof(float), out);

	fclose(out);
	return 0;
}

