import numpy as np
import collections
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import csv
from matplotlib import rc
import pandas as pd
# from load import *

### rcParams are the default parameters for matplotlib
import matplotlib as mpl
rc('mathtext',default='regular')
fonts = 25;
fonts_title = 25
leng_fontsize = 25
mpl.rcParams['font.size'] = fonts
mpl.rcParams['font.family'] = 'Times New Roman'
mpl.rcParams['axes.labelsize'] = fonts
mpl.rcParams['xtick.labelsize'] = fonts
mpl.rcParams['ytick.labelsize'] = fonts
mpl.rcParams['axes.linewidth'] = 0
mpl.rcParams['ytick.major.pad']='8'
mpl.rcParams['xtick.major.pad']='10'

xpos = -0.4
ypos = 0.5
lblsize = 20
boldness = 500
ticklblsize = 20
transparency = 1.0


labels = ['CTL', 'INaB', 'IKurB', 'INaB+IKurB', 'Y155C', 'D469E', 'P488S'];
files = ['None', 'D322H', 'E48G', 'A305T', 'Y155C', 'D469E', 'P488S'];
lnwidth = 0.8*np.array( [3.5, 		3.5 ,     3.5,       3.5,      5,      3.5,          3.5]);
colors = ['0.20',     '#7CFC00',    'b',     'r',   '#00008B', 'cyan',  'magenta'];
markers = 7*'v'
ls=['-', '--', '-', '--']
fig = plt.figure(figsize=(19/2.54,19/2.54))
num_row = 1;
num_col = 1;
gs1 = gridspec.GridSpec(num_row,num_col,
	# width_ratios=[1.,1],
 #    height_ratios=[1,1]
	);
panel = {};

for i in np.arange(num_row):
	for j in np.arange(num_col):
		panel[i,j] = plt.subplot(gs1[i,j]);
		ax = panel[i,j];

# CRN_AP=[]
# CRN_AP.append( pd.read_csv('90.vtk', sep=" ", header = None))

# file = 'v7398.bin'

import sys

file = 'v'+sys.argv[1]+'.bin'
CRN_AP = np.fromfile(file, dtype=np.float32)


print len(CRN_AP)
# print CRN_AP[0][0]
CRN_AP=np.reshape(CRN_AP, (251, 251))

print len(CRN_AP)
# ax.contourf(CRN_AP, 50)
cax=ax.pcolor(CRN_AP, vmax=5, vmin=-82)
plt.gca().set_aspect('equal')

plt.colorbar(cax,ticks=[-82, 5], orientation='horizontal')
print 'max', np.max(CRN_AP)
print 'min',np.min(CRN_AP)
# plt.axis_equal()


for i in np.arange(num_col*num_row):
	ax = fig.axes[i]
	ax.autoscale(enable=True, axis='x')  
	ax.autoscale(enable=True, axis='r')  
	ax.spines['right'].set_visible(False)
	#ax.spines['left'].set_visible(False)
	ax.spines['top'].set_visible(False)
	ax.spines['bottom'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.spines['bottom'].set_linewidth(0)
	ax.spines['left'].set_linewidth(0)
	ax.spines['right'].set_linewidth(0)
	ax.yaxis.tick_left()
	# # plt.xticks(np.linspace(0,600, 5))
	# ax.set_xticks([0, 250, 500])
	ax.yaxis.set_tick_params(width=0)
	ax.xaxis.set_tick_params(width=0)
ax.set_xlim(0,250)
ax.set_ylim(0,250)
ax.set_xticklabels([])
ax.set_yticklabels([])
plt.tight_layout()



plt.savefig(file+'.png', dpi=300)
plt.show()