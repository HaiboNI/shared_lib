import scipy.optimize as optimize
import numpy as np
import collections
import matplotlib.pyplot as plt
import csv


filein = 'Y155C.txt'
# filein = 'None_APD.dat'
data = np.loadtxt(filein,unpack=False)#delimiter='\t'


num = len(data)


out_name = filein.split('.')[0]+'.DI.'+ filein.split('.')[1]

with open(out_name, 'w') as fileout:
	for i in range(num):
		print >> fileout, data[i][0] - data[i][1],  data[i][2]
		print >>fileout,  data[i][0] - data[i][2],  data[i][1]