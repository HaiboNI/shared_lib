% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_AS_1.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0; 
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_AS_1.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_AS_1.data -ascii answer;

% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_AS_2.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0;
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_AS_2.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_AS_2.data -ascii answer;

% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_IVC.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0;
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_IVC.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_IVC.data -ascii answer;

% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_LA_1.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0;
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_LA_1.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_LA_1.data -ascii answer;


% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_LA_2.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0;
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_LA_2.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_LA_2.data -ascii answer;


% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_LAA.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0;
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_LAA.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_LAA.data -ascii answer;


% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_PM.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0;
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_PM.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_PM.data -ascii answer;


% SK. Dec. 2012.
% MATLAB script to get frequency where psd is maximum.
clear all;
clear all;
close all;
close all;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% make your input file called traces.dat, or change the file name here.
% the file is ASCII with columns: voltage1 voltage2 voltage3 ...
data = load('Action_potentials_RAA_1.txt');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sampling frequency is Fs in s^-1. This is from Mike.
Fs = 1e3;
% time vector till t_final - dt
t = 0:1/Fs:1-(1/Fs);
x = data(:,2); % the signal. If you want the third column, do: x = data(:,3);
psdest = psd(spectrum.periodogram,x,'Fs',1e3,'NFFT',length(x));
% plot(psdest.Frequencies,psdest.Data); % if you are debugging, uncomment this.

% copy the output to something you can change.
psd_data = psdest.Data;
psd_freq = psdest.Frequencies;
% rid of the first non-oscillating term entry.
psd_data(1) = 0.0;
% put the answer into my_data.
psddata = [psd_freq psd_data/max(psd_data)];
clear psd_data;
clear psd_freq;
figure;
% this works because both vectors are organised according to Freq.
plot(psddata(:,1),psddata(:,2)); % in case you want to see it. read the peak off this plot.
%
% save the psddata.
save psddata_RAA.data -ascii psddata;
% now do the cycle length.
%
% this is the return map, which you may or may not need.
%
%
j = 1;
time = data(:,1);
for i=2:1:length(time)
    if x(i-1)<-70.0&x(i)>=-70.0 % mess with the crossing threshold if you need.
        upstroke(j) = time(i);
        j = j + 1;
    end;
end;

for i=2:1:length(upstroke)
    cln(i)      = upstroke(i) - upstroke(i-1);
end;

% cln(1) = cln(2);
cln(1) = 120.0; % to make it uniform for the CL_n+1 vs CLn plot.

for i=1:1:length(upstroke)-1
    clnplus1(i) = upstroke(i+1) - upstroke(i);
end;

clnplus1(length(upstroke)) = clnplus1(length(upstroke)-1);
% clnplus1(length(upstroke)) = 120.0;

% the answer has a bit too much. so a manual edit may be required.
answer = [cln(1:length(cln))' clnplus1(1:length(cln))'];
figure;
plot(answer(:,1),answer(:,2));
% save the anwer.
save returnmap_RAA_1.data -ascii answer;

