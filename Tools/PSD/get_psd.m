function [PSD, F] = get_psd(data_vec, Fs)

	% N = length(data_vec);
	[PSD,F]  = pwelch(data_vec,ones(length(data_vec),1),[],length(data_vec),Fs,'psd');
	PSD(1) = 0.0;  % suppressing the constant zero-pad
	figure,
	plot(F,PSD);
	set(gca,'FontSize',14,'fontWeight','bold')
	title('PSD analysis');
	xlabel('Freq (Hz)')
	ylabel('Power Density')
	set(findall(gcf,'type','text'),'FontSize',14,'fontWeight','bold')
end 