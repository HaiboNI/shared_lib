# this is a python script to run the code

# the c++/c program output an ratio of amplitude of AP arising from S2/S1




from subprocess import call
import math
# S1 = 500
# S2 = 250

# d_S2 = 5;
mut = "WT"
# ISO = "1"
def run(S1, S2, Mode):
	global mut
	#call(["./main","BCL", str(S1), "S2", str(S2), "Mutation", mut, "S1_number", "20", "ISO", ISO])  #
	call(["./main", str(S1), "14", str(S2), 'WT', Mode]) #"Mutation", mut, "S1_number", "20", "ISO", ISO])  #
	with open('ratio.log',"r") as f:
		line = f.readlines()
		a = True
		# print float(line)
		value= float(line[0])
		if value > 0.8:
			a = True
		else:
			a = False

	return a, value



def find_ERP(S1, S2, d_S2):
	f = open('workfile', 'a+')
	compute=True;
	if S2 > S1:
		S2 = S1
	# for the first run, get Initial conditions
	go,s=run(S1, S2, 'Normal')
	lastgo=go
	print "*"*50
	print  "S1= ", S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "go = ", go
	print "*"*50
	while compute:
		if(lastgo == (not go) ):
			d_S2=d_S2/2.0

		if go:
			S2 =S2-d_S2
		else:
			S2 =S2+d_S2
		lastgo=go
		# for the following runs, concerntrate on the S2, so run with restart.
		go,s=run(S1, S2, 'Restart')
		# print  >>  f, mut, "S1= ", S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "go = ", go
		print "*"*50
		print  "S1= ", mut, S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "go = ", go
		print "*"*50
		if math.fabs(d_S2)<0.5 and math.fabs(s-0.8)<0.005:
			compute = False
	return S2

	# return a, line, line2


def find_upper(S1, S2, d_S2):
	f = open('workfile', 'a+')
	compute=True;
	go,s,e=run(S2)
	lastgo=go
	print "*"*50
	print  "S1= ", S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "end = ", e,"go = ", go
	print "*"*50
	while compute:
		if(lastgo == (not go) ):
			d_S2=d_S2/2.0

		if go:
			S2 =S2-d_S2
		else:
			S2 =S2+d_S2
		lastgo=go
		go,s,e=run(S2)
		print  >>  f, mut, "S1= ", S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "end = ", e,"go = ", go
		print "*"*50
		print  "S1= ", mut, S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "end = ", e,"go = ", go
		print "*"*50
		if math.fabs(d_S2)<0.05:
			compute = False
	return S2


def find_lower(S1, S2, d_S2):
	f = open('workfile', 'a+')
	compute=True;

	go,s,e=run(S2)

	if s=='1' and e=='1':
		go=False
	else:
		go = True
	lastgo=True
	print "*"*50
	print  "S1= ", S1, mut, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "end = ", e,"go = ", go
	print "*"*50
	while compute:
		if(lastgo == (not go) ):
			d_S2=d_S2/2.0

		if go:
			S2 =S2-d_S2
		else:
			S2 =S2+d_S2
		lastgo=go
		go,s,e=run(S2)
		if s=='1' and e=='1':
			go=False
		else:
			go = True
		print  >>  f, mut, "S1= ", S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "end = ", e,"go = ", go
		print "*"*50
		print  "S1= ", mut, S1, "S2= ", S2, "d_S2 = ", d_S2, "start = ", s, "end = ", e,"go = ", go
		print "*"*50

		if math.fabs(d_S2)<0.05:
			compute = False
	return S2

# upper = find_upper(S1, 305, 10)
# lower = find_lower(S1,upper,10)


# run(250)



call(["make"])  #
file= open('ERP.dat', 'a+')


find_ERP(1000, 309.375, 5)

# S2 = 390;
# for S1 in range(1200, 320-20, -20):
# 	# print >> file, S1
# 	S2 = find_ERP(S1, S2, 5);
# 	print >> file,S1, S2


file.close()


# f = open('limit.dat', 'w')

# print >> f, mut, "upper" , upper
# print >> f, mut, "lower" , lower
