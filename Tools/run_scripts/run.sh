#!/bin/bash

echo $1 $2 $2
mkdir $1
./Ventricle_3D BCL 500 Stim_type Paced Total_time 500  Geometry_File $2 Apicobasal_File $3 Stim_Time_File Geometry/manual_stim_points.150428-0.time.bin Stim_Amp_File Geometry/manual_stim_points.150428-0.amp.bin &> log.txt

mv *.bin $1
mv log.txt $1