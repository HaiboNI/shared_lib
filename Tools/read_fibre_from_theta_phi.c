
// #include "util.c"
// #include "conduction.c"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <zlib.h>



float *** allocate_and_zero_3d_float(int nz, int ny, int nx) {
// allocates and zeros a 3D array of floats of the given dimensions.
// nz is the slowest dimension, nx the fastest.

    float *** array;
    char error_message[256];
    int k, j, i;

    // allocate the backbone of our array
    array = malloc(nz * sizeof(float**));
    if (!array) {
        sprintf(error_message, "malloc: error allocating z spine!");
        perror(error_message);
        exit(EXIT_FAILURE);
    }

    for (k = 0; k < nz; ++k) {
        // null it first.
        array[k] = NULL;

        // allocate a pointer to hold the y rows
        array[k] = malloc(ny * sizeof(float*));
        if (!array[k]) {
            sprintf(error_message, "malloc: error allocating y spur %d", k);
            perror(error_message);
            exit(EXIT_FAILURE);
        }

        for (j = 0; j < ny; ++j) {
            array[k][j] = NULL;

            // allocate a pointer to hold the y rows
            array[k][j] = malloc(nx * sizeof(float));
            if (!array[k][j]) {
                sprintf(error_message, "malloc: error allocating x rows %d, %d", k, j);
                perror(error_message);
                exit(EXIT_FAILURE);
            }

            for (i = 0; i < nx; ++i) {
                array[k][j][i] = 0;
            }
        }
    }

    return array;
}

void deallocate_and_zero_3d_float(float *** array, int nz, int ny, int nx) {
// deallocates and zeros a 3D array of floats of the given dimensions.
// nz is the slowest dimension, nx the fastest.
    int k, j, i;
    if (array) {
        for (k = 0; k < nz; ++k) {
            if (array[k]) {
                for (j = 0; j < ny; ++j) {
                    if (array[k][j]) {
                        for (i = 0; i < nx; ++i) {
                            array[k][j][i] = 0;
                        }
                        free(array[k][j]);
                        array[k][j] = NULL;
                    }
                }
                free(array[k]);
                array[k] = NULL;
            }
        }
        free(array);
        array = NULL;
    }
}


unsigned char *** allocate_and_zero_3d_unsigned_char(int nz, int ny, int nx) {
    // allocates and zeros a 3D array of unsigned chars of the given dimensions.
    // nz is the slowest dimension, nx the fastest.
    unsigned char *** array;
    char error_message[256];
    int k, j, i;

    // allocate the backbone of our array
    array = malloc(nz * sizeof(unsigned char**));
    if (!array) {
        sprintf(error_message, "malloc: error allocating z spine!");
        perror(error_message);
        exit(EXIT_FAILURE);
    }

    for (k = 0; k < nz; ++k) {
        // null it first.
        array[k] = NULL;

        // allocate a pointer to hold the y rows
        array[k] = malloc(ny * sizeof(unsigned char*));
        if (!array[k]) {
            sprintf(error_message, "malloc: error allocating y spur %d", k);
            perror(error_message);
            exit(EXIT_FAILURE);
        }

        for (j = 0; j < ny; ++j) {
            array[k][j] = NULL;

            // allocate a pointer to hold the y rows
            array[k][j] = malloc(nx * sizeof(unsigned char));
            if (!array[k][j]) {
                sprintf(error_message, "malloc: error allocating x rows %d, %d", k, j);
                perror(error_message);
                exit(EXIT_FAILURE);
            }

            for (i = 0; i < nx; ++i) {
                array[k][j][i] = 0;
            }
        }
    }

    return array;
}



void deallocate_and_zero_3d_unsigned_char(unsigned char *** array, int nz, int ny, int nx) {
// deallocates and zeros a 3D array of unsigned chars of the given dimensions.
// nz is the slowest dimension, nx the fastest.
    int k, j, i;
    if (array) {
        for (k = 0; k < nz; ++k) {
            if (array[k]) {
                for (j = 0; j < ny; ++j) {
                    if (array[k][j]) {
                        for (i = 0; i < nx; ++i) {
                            array[k][j][i] = 0;
                        }
                        free(array[k][j]);
                        array[k][j] = NULL;
                    }
                }
                free(array[k]);
                array[k] = NULL;
            }
        }
        free(array);
        array = NULL;
    }
}




unsigned char *** read_and_embed_geometry(char * filename, int nz, int ny, int nx) {
// reads an unsigned character geometry from a (possibly gzip'd) file called
// filename and embeds it in a geometry that is one larger in all directions.
    int j, k, rw;
    gzFile gz;

    unsigned char *** embedded_array = allocate_and_zero_3d_unsigned_char(nz + 2, ny + 2, nx + 2);

    // open file and test
    gz = gzopen(filename, "r");
    if (!gz) {
        perror(filename);
        deallocate_and_zero_3d_unsigned_char(embedded_array, nz + 2, ny + 2, nx + 2);
        exit(EXIT_FAILURE);
    }

    // read in the file, one x row at a time.
    for (k = 1; k < nz + 1; k++) {
        for (j = 1; j < ny + 1; j++) {
            rw = gzread(gz, embedded_array[k][j] + 1, nx);
            if (rw != nx) {
                fprintf(stderr, "%d/%d bytes read from '%s' at (%d,%d), exiting!\n",
                        rw, nx, filename, k, j);
                deallocate_and_zero_3d_unsigned_char(embedded_array, nz + 2, ny + 2, nx + 2);
                exit(EXIT_FAILURE);
            }
        }
    }

    // close gz file and free memory
    gzclose(gz);

    return embedded_array;
}



void calculate_fibre_unit_vectors(unsigned char *** theta, unsigned char *** phi,
                                  int nz, int ny, int nx, float ***z, float ***y, float ***x) {
    int i, j, k;
    float t, p;

    for (k = 1; k < nz + 1; ++k) {
        for (j = 1; j < ny + 1; ++j) {
            for (i = 1; i < nx + 1; ++i) {
                if (phi[k][j][i] != 255) {
                    t = (float) ((theta[k][j][i] / 254.0) * M_PI);
                    p = (float) ((phi[k][j][i] / 254.0) * M_PI);

                    x[k][j][i] = (float) (cos(p) * sin(t));
                    y[k][j][i] = (float) (sin(p) * sin(t));
                    z[k][j][i] = (float) cos(t);
                } else {
                    x[k][j][i] = 0.0;
                    y[k][j][i] = 0.0;
                    z[k][j][i] = 0.0;
                }
            }
        }
    }
}


#define nz 300
#define ny 271
#define nx 239

int main(int argc, char const *argv[])
{
    /* code */


    char *theta_name, * phi_name;
    char *fibre_x_file, *fibre_y_file, *fibre_z_file;

    theta_name   = (char *) malloc(30 * sizeof(char));
    phi_name     = (char *) malloc(30 * sizeof(char));
    fibre_x_file = (char *) malloc(30 * sizeof(char));
    fibre_y_file = (char *) malloc(30 * sizeof(char));
    fibre_z_file = (char *) malloc(30 * sizeof(char));
    // sprintf(theta_name, "ATRIUM.theta.gz");
    // sprintf(phi_name, "ATRIUM.phi.gz");

    sprintf(theta_name, "Last_Update_Atria_Geo_Full_Fibre.theta.gz");
    sprintf(phi_name, "Last_Update_Atria_Geo_Full_Fibre.phi.gz");
    // sprintf(fibre_x_file, "New_Atria_Geo_Full_Fibre_X.geo.gz");
    // sprintf(fibre_y_file, "New_Atria_Geo_Full_Fibre_Y.geo.gz");
    // sprintf(fibre_z_file, "New_Atria_Geo_Full_Fibre_Z.geo.gz");

    unsigned char *** theta  = read_and_embed_geometry(theta_name, nz, ny, nx);
    // printf("aa");
    unsigned char *** phi    = read_and_embed_geometry(phi_name,   nz, ny, nx);
    // printf("a");

    float ***x = allocate_and_zero_3d_float(nz + 2, ny + 2, nx + 2);
    float ***y = allocate_and_zero_3d_float(nz + 2, ny + 2, nx + 2);
    float ***z = allocate_and_zero_3d_float(nz + 2, ny + 2, nx + 2);
    calculate_fibre_unit_vectors(theta, phi, nz, ny, nx, z, y, x);

    // float ***x = read_and_embed_float_data(fibre_x_file, nz, ny, nx);
    // float ***y = read_and_embed_float_data(fibre_y_file, nz, ny, nx);
    // float ***z = read_and_embed_float_data(fibre_z_file, nz, ny, nx);

    {
        FILE *out;
        out = fopen ("MIKE_atrium_fibre.vtk", "wt");

        fprintf(out, "# vtk DataFile Version 3.0\n");
        fprintf(out, "vtk output\n");
        fprintf(out, "ASCII\n");
        fprintf(out, "DATASET STRUCTURED_POINTS\n");
        fprintf(out, "DIMENSIONS %d %d %d\n", nx, ny, nz);
        fprintf(out, "SPACING 1 1 1\n");
        fprintf(out, "ORIGIN 0 0 0\n");
        fprintf(out, "POINT_DATA %d\n", (nz ) * (ny ) * (nx));
        fprintf(out, "SCALARS HumanAtrium float 3\n");
        fprintf(out, "LOOKUP_TABLE default\n");

        int i, j, k;
        float t, p;

        for (k = 1; k < nz + 1; ++k) {
            for (j = 1; j < ny + 1; ++j) {
                for (i = 1; i < nx + 1; ++i) {

                    fprintf(out, "%f %f %f ", x[k][j][i], y[k][j][i], z[k][j][i]);

                }
                fprintf(out, "\n");
            }
        }
        fclose(out);
    }
    return 0;
}
