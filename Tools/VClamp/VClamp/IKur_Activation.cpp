#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "NewIKur.hpp"


int main(int argc, char const *argv[])
{
	int mutation = 0;
	int count = 0;

	//general parameters
	double dt = 0.02;
	double V, Vhold, Vstart, Vend, Vstep, Vstep_size, Vstep_end;
	double clamp_time;
	double peak_current_pos, peak_current_neg, end_current;
	FILE *volt_out, *current_out;
	double I, t;
	int end_switch = 0;

	// define values for general parameters
	Vhold = -80;
	Vstart = -40;
	Vend = -40;
	Vstep_end = 40;

	clamp_time = 250;
	double Hold_time = 2000;
	double End_time = 100;
	Vstep_size = 10;

	//file names
	volt_out = fopen("Act_I-V_relationship.txt", "wt");
	current_out = fopen("Act_current_trace.txt", "wt");
	double state[3] = {0};



	//do voltage clamp and calculate current

	for (Vstep = Vstart; Vstep < Vstep_end + 1; Vstep = Vstep + Vstep_size)
	{
		// initial conditions for gating variables (reset before each step)
		// double ac = ((IKur_ac1_mult * 1.0) / (1 + exp((Vhold + 17.6684 + IKur_ac1_shift) / (-5.75418 * IKur_ac1_grad))) ) * ((IKur_ac2_mult * 1.0) / (1 + exp((Vhold + 8.4153 + IKur_ac2_shift) / (-11.51037561 * IKur_ac2_grad)))) + IKur_ac_add;

		init_IKur(state, Vhold);

		// double inac = (IKur_inac_mult * 0.52424) / (1.0 + exp((Vhold + 15.1142 + IKur_inac_shift) / (7.567021 * IKur_inac_grad))) + 0.4580778 + IKur_inac_add;
		// step = 30;

		peak_current_pos = peak_current_neg = 0.0;
		end_switch = 0;

		count = 0;

		for (t = 0.0; t < Hold_time + clamp_time + End_time; t = t + dt)
		{
			count ++;

			if (t < Hold_time) V = Vhold;
			else if (t < Hold_time + clamp_time) V = Vstep;
			else
			{
				V = Vend;

			}

				if (t <=  Hold_time + clamp_time - 3*dt && t >  Hold_time + clamp_time - 4*dt)
				{
					end_current = I;
				}

			double room = 1.0;
			I = Updated_IKur(state, V, dt, room);

			//calculate peak current values
			if (t > Hold_time + clamp_time-10)
			{
				if (I > peak_current_pos) peak_current_pos = I;
				if (I < peak_current_neg) peak_current_neg = I;
			}
			if ((count % 10) == 0)
			{
				double ac     = state[0];
				double inac_f = state[1];
				double inac_s = state[2];
				fprintf(current_out, "%f %f %f %f %f %f\n", t, I , V, ac, inac_f, inac_s);
			}
			// fprintf(current_out, "%f %f %f\n", t, I / Cm, V);
		} // end time for loop

		// for steady state and tau, include equations here with Vstep as the V term

		fprintf(volt_out, "%f %f %f %f\n", Vstep, peak_current_pos, peak_current_neg, end_current);
	} // end for V loop

} // end main



