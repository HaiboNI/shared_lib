	


#include <cmath>

template <typename temp_type>
inline temp_type Foward_Euler(temp_type dt, temp_type dy, temp_type y) {
    /*temp_type Y;*/
    return (y + (dt) * dy);
}

template <typename temp_type>
inline temp_type Euler_inf(temp_type dt, temp_type gate, temp_type inf, temp_type tau) {
    return (inf + (gate - inf) * exp(-(dt) / tau));
}




void init_CRN_IKur_2017(double state[3], double V) {
	double ac = pow(1.0 + exp((V - (-10.0) + 20.3) / (-9.6)), -1.0);
	double inac_f =  1.0 / (1.0 + exp((V - 35) / 20.0));
	double inac_s = 1.0 / (1.0 + exp((V + 5.0) / 5.0));
	state[0] = ac    ;
	state[1] = inac_f;
	state[2] = inac_s;


}
double CRN_IKur_2017(double state[3], double V, double dt, double room) {

	double K_Q10 = 3.0;


	double alpha_ua = 0.65 * pow(exp((V - (-10.0)) / -8.5) + exp((V - (-10.0) - 40.0) / -59.0), -1.0);
	double beta_ua = 0.65 * pow(2.5 + exp((V - (-10.0) + 72.0) / 17.0), -1.0);
	double tau_ua = pow(alpha_ua + beta_ua, -1.0) / K_Q10;
	double ua_infinity = pow(1.0 + exp((V - (-10.0) + 20.3) / (-9.6)), -1.0);
	// dstate[19] = (ua_infinity-state[19])/tau_ua;
	state[0] = Euler_inf(dt, state[0], ua_infinity, tau_ua);

	double alpha_ui = pow(21.0 + 1.0 * exp((V - (-10.0) - 195.0) / -28.0), -1.0);
	double beta_ui = 1.0 / exp((V - (-10.0) - 168.0) / -16.0);
	double tau_ui = pow(alpha_ui + beta_ui, -1.0) / K_Q10;



	// Update from the Aguilar et al. Biophysical journal paper.
	// fast u inactivation
	double ui_infinity = 1.0 / (1.0 + exp((V - 35) / 20.0));
	// ui_infinity = 1.0 / (1.0 + exp((V + 5.0) / 5.0));
	
	tau_ui = 800.0 * (2 - V / 40.0);
	/*if (V<-60)
	{
		tau_ui = 500;
	}*/
	state[1] = Euler_inf(dt, state[1], ui_infinity, tau_ui);


	// dstate[20] = (ui_infinity-state[20])/tau_ui;

	// slow u inactivation
	ui_infinity = 1.0 / (1.0 + exp((V + 5.0) / 5.0));
	tau_ui = 5800.0 / (1.0 + exp(-(V + 80.0) / 11.0));
	double F = 96.4867;   // coulomb_per_millimole (in membrane)
	double R = 8.3143;   // joule_per_mole_kelvin (in membrane)
	double T = 310.0;   // kelvin (in membrane)
	state[2] = Euler_inf(dt, state[2], ui_infinity, tau_ui);
	double g_Kur =  0.005 + 0.05 / (1.0 + exp((V - 15.0) / -13.0));
	double E_K = R * T / F * log(5.4 / 140.0);
	double i_Kur = g_Kur * pow(state[0], 3.0) * state[1] * state[2] * (V - E_K);
	return i_Kur;
}


class CELL
{
public:
	CELL(){};
	~CELL(){};
	double V, I, I_Pre;
};