#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	//define constants
	double    R = 8.3143;
	double    T = 293.0;
	double    F = 96.4867;
	double    K_o = 5.4;
	double    K_i = 138.4;
	double    Cm = 100;

	int count = 0;
	double GKur = 1.0;
	double Ikur_cnd_add = 0.0;
	double Ikur_cnd_mult = 1.0;
	double Ikur_cnd_shift = 0.0;
	double Ikur_cnd_grad = 1.0;
	double IKur_ac1_mult = 1.0;
	double IKur_ac1_shift = 0.0;
	double IKur_ac1_grad = 1.0;
	double IKur_ac2_mult = 1.0;
	double IKur_ac2_shift = 0.0;
	double IKur_ac2_grad = 1.0;
	double IKur_ac_add = 0.0;
	double IKur_inac_mult = 1.0;
	double IKur_inac_shift = 0.0;
	double IKur_inac_grad = 1.0;
	double IKur_inac_add = 0.0;

	double IKur_ac_shift = 0.0;
	double IKur_ac_grad = 1.0;
	double CNZ_conductance_scaleF = 1.0;

	double IKur_K0 = -8.26597;
	double IKur_c = 4.5128;
	double IKur_x0 = 1.899769;
	double IKur_y0 = 20.5232;
	
// (4.5128 + 1.899769 / (1.0 + exp((V - 20.5232) / (-8.26597))))
	int mutation = 0;




	//general parameters
	double dt = 0.02;
	double V, Vhold, Vstart, Vend, Vstep, Vstep_size, Vstep_end;
	double clamp_time;
	double peak_current_pos, peak_current_neg, end_current;
	FILE *volt_out, *current_out;
	double I, t;
	int end_switch = 0;

	// define values for general parameters
	Vhold = -80;
	Vstart = -40;
	Vend = -40;
	Vstep_end = 40;

	clamp_time = 250;
	Vstep_size = 10;

	//file names
	volt_out = fopen("NewIKur_I-V_relationship.txt", "wt");
	current_out = fopen("NewIKur_current_trace.txt", "wt");

	// define and set up specific current parameters
	//double gmax = ;
	double EK = 26.71 * log(K_o / K_i);
	double vshift = 0;
	double Tfac = 3;

	//do voltage clamp and calculate current

	for (Vstep = Vstart; Vstep < Vstep_end + 1; Vstep = Vstep + Vstep_size)
	{
		// initial conditions for gating variables (reset before each step)
		// double ac = ((IKur_ac1_mult * 1.0) / (1 + exp((Vhold + 17.6684 + IKur_ac1_shift) / (-5.75418 * IKur_ac1_grad))) ) * ((IKur_ac2_mult * 1.0) / (1 + exp((Vhold + 8.4153 + IKur_ac2_shift) / (-11.51037561 * IKur_ac2_grad)))) + IKur_ac_add;

		double ac = 1 / (1 + exp(-(Vhold +  5.52) / (8.81 )));

		double inac_f = 1.0 / (1.0 + exp( (Vhold+7.5)/6.67));
		double inac_s = 1.0 / (1.0 + exp( (Vhold+7.5)/6.67));




		// double inac = (IKur_inac_mult * 0.52424) / (1.0 + exp((Vhold + 15.1142 + IKur_inac_shift) / (7.567021 * IKur_inac_grad))) + 0.4580778 + IKur_inac_add;


		// step = 30;

		peak_current_pos = peak_current_neg = 0.0;
		end_switch = 0;

		count = 0;

		for (t = 0.0; t < 3 * clamp_time; t = t + dt)
		{
			count ++;

			if (t < clamp_time) V = Vhold;
			else if (t < 2 * clamp_time) V = Vstep;
			else
			{
				V = Vend;
				// end_current = I;
				if (end_switch <= 5)
				{
					end_switch += 1;
				}

				if (end_switch == 1)
				{
					end_current = I;
				}
			}

			double const  CNZ_gkur = 0.006398;
			double K_Q10 = 3.0;
			double room = 3.0;
			//CNZ_a
			// double inf = ((IKur_ac1_mult * 1.0) / (1 + exp((V + 17.6684 + IKur_ac1_shift) / (-5.75418 * IKur_ac1_grad))) ) * ((IKur_ac2_mult * 1.0) / (1 + exp((V + 8.4153 + IKur_ac2_shift) / (-11.51037561 * IKur_ac2_grad)))) + IKur_ac_add;
			double inf =  1 / (1 + exp(-(V +  5.52) / (8.81 )));

			double tau = (45.67/(1.0 + exp((V+9.23)/10.53)) + 4.27) *( 0.55/(1.0 + exp((V+60.87)/-22.88))+0.03);
			tau = tau / K_Q10*room;

			ac = inf + (ac - inf) * exp(-(dt) / tau);

			// CNZ_if
			inf = 1.0 / (1.0 + exp( (V+7.5)/6.67));
			tau = 1300/(exp((V-133.0)/97.0)*5.04 + 0.2*exp((V+10.9233071)/(-12.0)))+370.0;   // at 37deg
			tau = tau*room;
			inac_f = inf + (inac_f - inf) * exp(-(dt) / tau);
			// CNZ_is
			inf = 1.0 / (1.0 + exp( (V+7.5)/6.67));
			tau = 1400/(exp((V+0.1)/8.86)+1.0)+ 478.0/(exp((V-41.0)/-3.0)+1.0) +6500.0;
			tau = tau*room;
			inac_s = inf + (inac_s - inf) * exp(-(dt) / tau);


			// I = CNZ_conductance_scaleF * GKur * Cm * CNZ_gkur * (4.5128 + 1.899769 / (1.0 + exp((V - 20.5232) / (-8.26597)))) * ac * inac * (V - EK);
			I = CNZ_conductance_scaleF * GKur * Cm * CNZ_gkur * (IKur_c + IKur_x0 / (1.0 + exp((V - IKur_y0) / (IKur_K0)))) * ac * (inac_f*0.65 + inac_s*0.35) * (V - EK);


			//calculate peak current values
			if ( V == Vstep)
			{
				if (I > peak_current_pos) peak_current_pos = I;
				if (I < peak_current_neg) peak_current_neg = I;
			}
			if ((count % 10) == 0)
			{
				fprintf(current_out, "%f %f %f %f %f %f\n", t, I / Cm, V, ac, inac_f, inac_s);
			}
			// fprintf(current_out, "%f %f %f\n", t, I / Cm, V);
		} // end time for loop

		// for steady state and tau, include equations here with Vstep as the V term

		fprintf(volt_out, "%f %f %f %f\n", Vstep, peak_current_pos / Cm, peak_current_neg / Cm, end_current / Cm);
	} // end for V loop

} // end main

