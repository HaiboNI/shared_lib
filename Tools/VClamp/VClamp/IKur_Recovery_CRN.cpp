#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <iomanip> // for 
using namespace std;
#include <string>     // std::string, std::to_string

#include <vector>
//  // #define OUT_TRACE
#include <stdio.h>
// #include "NewIKur.hpp"
#include "CRN_IKur_2017.hpp"

#define OUT_TRACE


void MyRFI( double Rec_Hold);
int main(int argc, char const *argv[])
{
	MyRFI(-80.0);
} // end main




void MyRFI( double Rec_Hold)
{

	int i;

	double P1_P2_step_V = 40.0;
	double room = 1.0;

	CELL Cell;
	int counter = 0;
	double t = 0, dt = 0.02, tTemp = 0;
	double V_cycle, V_rec;

	//char word[20];
	//cout << "To begin, enter file names:" << endl<< endl;
	//scanf ("%s", word);

	string str = "CRN_";
	char temp_str1[100];
	sprintf (temp_str1, "%.0f", Rec_Hold);
	string temp_str;
	temp_str = temp_str1;
	//cout << "Thanks!"<< endl;
	//chdir ("results");

	ofstream result_file((str + ".txt").c_str()   );
	ofstream result_file2((str + "RFI" + temp_str + "_param.txt").c_str()   );



	//general parameters
	double V, Vhold, Vstart, Vend, Rec_Hold_size, Rec_Hold_end;
	double clamp_time;
	double peak_current_pos, peak_current_neg, end_current;
	// FILE *volt_out, *current_out;
	int end_switch = 0;

	// define values for general parameters
	Vhold = -140.0;
	Vstart = -100.0;
	Vend = -100.0;
	Rec_Hold_end = -60;

	clamp_time = 100;
	Rec_Hold_size = 10.0;

	dt = 0.02;

	const double pause = 2.0;

	std::vector<double> P2_time;
	P2_time.push_back(0);
	for (int i = 1; i <= 31; ++i)
	{
		P2_time.push_back(pow(2.0, i / 2.0));
	}

	t = 0.0;

	int Protocol = 1;

	int count;
	// interval = t_hold + 150.0;
	int cycle = 0;
	double P1_time = 10000.0;

	// Rec_Hold = 30;


	double waitTime = 1000.0;

	std::cout << P2_time.size() << std::endl;


	double state[3];
	init_CRN_IKur_2017(state, Vhold);
	for (int i = 0; i < P2_time.size(); ++i)
	{
		// std::cout << i << std::endl;

		// holding the cell by 1000. ms
		/*for (t = 0; t < waitTime; t += dt)
		{

			Cell.I = Updated_IKur(state, Vhold, dt, room);

		}*/
		init_CRN_IKur_2017(state, Rec_Hold);


		peak_current_pos = peak_current_neg = 0.0;
		end_switch = 0;
		int flag_tau = 0;
		count = 0;
		counter = 0;
		cycle++;
		double I_peak = 0;
		double t_peak = 0;
		double t_tau;
		double norm;
		double I_peak_0;
		double tau_1_2, tau_1_4, tau_1_8, tau_1_16, tau_1_32, tau_1_64, tau_1_128;
		tau_1_2 =  tau_1_4 =  tau_1_8 =  tau_1_16 =  tau_1_32 =  tau_1_64 =  tau_1_128 = 0.0;
		for (tTemp = 0; tTemp <= P2_time[i] + 20.0 + P1_time; tTemp = tTemp + dt) {
			count ++;
			if (tTemp <= P1_time) {
				Cell.V = P1_P2_step_V;
			}
			else if (tTemp <= P2_time[i] + P1_time) {
				Cell.V = Rec_Hold;
			}
			else if (tTemp >  P2_time[i] + P1_time && tTemp <=  P2_time[i] + P1_time + 20.0) {
				Cell.V = P1_P2_step_V;  // hold at -20.0 to measure the maximum current
			} //0.005
			else {
				Cell.V = Rec_Hold;
			}

			Cell.I = CRN_IKur_2017(state, Cell.V, dt, room);


			if (tTemp <= P1_time)
			{
				if (I_peak_0 < Cell.I)
				{
					I_peak_0 = Cell.I;
				}
			}

			if ( tTemp < P2_time[i] ) {
				I_peak = 0;
			} //Resets I_peak just before the pulse to -10. Just in case you get some current at your holding step potential!

			// Calculate_peak(&Cell, tTemp, dt);

			if (tTemp > P2_time[i] + P1_time + dt && tTemp < P2_time[i] + P1_time + 20)
			{
				if (fabs(Cell.I) < fabs(Cell.I_Pre) and flag_tau == 0)
				{
					flag_tau = 1;
					t_peak = tTemp ;
					I_peak = Cell.I;
					result_file2  << std::setprecision(10)
					              << cycle << ", "
					              << Rec_Hold << ", "
					              << t_peak << ", "
					              << I_peak << ", "
					              << P2_time[i] << " ,"
					              << I_peak / (Rec_Hold - 66.722) << ", "
					              << I_peak /  I_peak_0  << ", "
					              << t_tau << ", "
					              << tTemp << ", "
					              // << Cell.availability << ", "
					              << endl;
				}
			}

			if (tTemp >= (P2_time[i] + 50.0 - dt) ) {
				if (cycle == 1) {
					// I_peak_0 = I_peak;
					norm = I_peak_0;// I_peak / (Rec_Hold - 66.722);
					// result_file2  << std::setprecision(10) << Rec_Hold << ", " << I_peak_0 << std::endl;
				}
				//Note, 66.722 is E_Na reversal potential, old code used 28.977 because I used log instead of ln in the Nernst equation,
				// (the original author of this code was stupid!!!!, creating loads
				// of unnecessary over-complicated code!!!!)

				//norm is the normalized current. Plot V_initial vs. norm to get the SSA curve
			}

#ifdef OUT_TRACE
			// if (tTemp > 4980 and tTemp < 5050)
			if (counter % 50 == 0) {
				result_file << tTemp  << " " << Cell.I << " " <<  state[1] << " "
				            << Cell.V << endl;
			}
#endif
			//Counter for t so you know where the program is
			//if (fmod(t, 5000) < dt) {cout << t  << ", " << cycle << ", " << tTemp << endl;}

			t = t + dt;
			counter++;


			Cell.I_Pre = Cell.I;



			// fprintf(current_out, "%f %f %f\n", t, I / Nygren_Cm, V);
		} // end time for loop

		// for steady state and tau, include equations here with Rec_Hold as the V term
		I_peak = 0;

		/* code */
	}

	result_file2.close();

	// return;
}


