#include <cmath>

double Updated_IKur(double *state, double V, double dt, double room) ;

void init_IKur(double *, double);



double Updated_IKur(double state[3], double V, double dt, double room) {


	double ac     = state[0];
	double inac_f = state[1];
	double inac_s = state[2];
	double I;

	//define constants
	double    R = 8.3143;
	double    T = 293.0;
	double    F = 96.4867;
	double    K_o = 5.4;
	double    K_i = 138.4;
	double    Cm = 100;
	// define and set up specific current parameters
	//double gmax = ;
	double EK = 26.71 * log(K_o / K_i);
	double vshift = 0;
	double Tfac = 3;
	double const  CNZ_gkur = 0.006398;
	double K_Q10 = 3.0;
	//CNZ_a
	// double inf = ((IKur_ac1_mult * 1.0) / (1 + exp((V + 17.6684 + IKur_ac1_shift) / (-5.75418 * IKur_ac1_grad))) ) * ((IKur_ac2_mult * 1.0) / (1 + exp((V + 8.4153 + IKur_ac2_shift) / (-11.51037561 * IKur_ac2_grad)))) + IKur_ac_add;
	double inf =  1 / (1 + exp(-(V +  5.52) / (8.81 )));

	double tau = (45.67 / (1.0 + exp((V + 9.23) / 10.53)) + 4.27) * ( 0.55 / (1.0 + exp((V + 60.87) / -22.88)) + 0.03);
	tau = tau / K_Q10 * room;

	ac = inf + (ac - inf) * exp(-(dt) / tau);

	// CNZ_if
	inf = 1.0 / (1.0 + exp( (V + 7.5) / 6.67));
	tau = 1300 / (exp((V - 133.0) / 97.0) * 5.04 + 0.2 * exp((V + 10.9233071) / (-12.0))) + 370.0; // at 37deg
	tau = tau * room;
	inac_f = inf + (inac_f - inf) * exp(-(dt) / tau);
	// CNZ_is
	inf = 1.0 / (1.0 + exp( (V + 7.5) / 6.67));
	tau = 1400 / (exp((V + 0.1) / 8.86) + 1.0) + 478.0 / (exp((V - 41.0) / -3.0) + 1.0) + 6500.0;
	tau = tau * room;
	inac_s = inf + (inac_s - inf) * exp(-(dt) / tau);

	// I = CNZ_conductance_scaleF * GKur * Cm * CNZ_gkur * (4.5128 + 1.899769 / (1.0 + exp((V - 20.5232) / (-8.26597)))) * ac * inac * (V - EK);
	I = CNZ_gkur * (4.5128 + 1.899769 / (1.0 + exp((V - 20.5232) / (-8.26597)))) * ac * (inac_f * 0.65 + inac_s * 0.35) * (V - EK);
	// I = CNZ_gkur * (4.5128 + 1.899769 / (1.0 + exp((V - 20.5232) / (-8.26597)))) * ac * (inac_f * inac_s) * (V - EK);

	state[0] = ac    ;
	state[1] = inac_f;
	state[2] = inac_s;

	return I;

}


void init_IKur(double * state, double V) {


	double ac     = 1 / (1 + exp(-(V +  5.52) / (8.81 )));
	double inac_f = 1.0 / (1.0 + exp( (V + 7.5) / 6.67));
	double inac_s = 1.0 / (1.0 + exp( (V + 7.5) / 6.67));
	state[0] = ac    ;
	state[1] = inac_f;
	state[2] = inac_s;

}


class CELL
{
public:
	CELL(){};
	~CELL(){};
	double V, I, I_Pre;
};