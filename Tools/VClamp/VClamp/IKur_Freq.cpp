#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <stdio.h>
#include "NewIKur.hpp"


int main(int argc, char const *argv[])
{




	//general parameters
	double dt = 0.01;
	double V, Vhold, Vstart, Vend, Vstep, Vstep_size, Vstep_end;
	double clamp_time;
	double peak_current_pos, peak_current_neg, end_current;
	FILE *volt_out, *current_out;
	double I, t;
	int end_switch = 0;


	int count=0;

	// define values for general parameters
	Vhold = -80;
	Vstart = -40;
	Vend = -40;
	Vstep_end = 40;
	Vstep = 40;
	clamp_time = 1000;
	Vstep_size = 10;

	//file names


	// define and set up specific current parameters
	//double gmax = ;

	//do voltage clamp and calculate current
	double state[3] = {0, 1, 0};
	int NUM = 100;

	double freq = atof(argv[1]);

	char file1[200];
	char file2[200];

	double StepTime = 100.0;

	sprintf(file1, "IV_.dat.%.0f.Hz", freq);
	sprintf(file2, "current_trace_.dat.%.0f.Hz", freq);

	volt_out = fopen(file1, "wt");
	current_out = fopen(file2, "wt");

	double single_swep_time = 1000.0 / freq;
	double global_time = 0;
	double time_end_step = 0.0;

	/*for (t = 0.0; t < 2000; t = t + dt)
	{
		V= Vhold;

		I = CNZ_IKur_Block(V,  dt,  state, 0);
	}*/

	double Rec_Hold = -80.0;
	init_IKur(state, Rec_Hold);
	for (int i = 0; i < NUM; ++i)
	{

		peak_current_pos = peak_current_neg = 0.0;
		end_switch = 0;

		count = 0;

		for (t = 0.0; t < single_swep_time; t = t + dt)
		{
			count ++;

			if (t < single_swep_time - StepTime - time_end_step) V = Vhold;
			if (t < single_swep_time - time_end_step and t >= single_swep_time - time_end_step - StepTime) V = Vstep;
			if (t > single_swep_time - time_end_step)
			{
				V = Vend;
				// end_current = I;
				if (end_switch <= 5)
				{
					end_switch += 1;
				}

				if (end_switch == 1)
				{
					end_current = I;
				}
			}

			// double const  CNZ_gkur = 0.006398;
			// double K_Q10 = 3.5308257834747638;
			// //CNZ_a
			// // double inf = ((IKur_ac1_mult * 1.0) / (1 + exp((V + 17.6684 + IKur_ac1_shift) / (-5.75418 * IKur_ac1_grad))) ) * ((IKur_ac2_mult * 1.0) / (1 + exp((V + 8.4153 + IKur_ac2_shift) / (-11.51037561 * IKur_ac2_grad)))) + IKur_ac_add;
			// double inf = ac = 1 / (1 + exp(-(V + IKur_ac_shift + 5.52) / (8.81 * IKur_ac_grad)));
			// double tau = (45.6666746826 / (1 + exp((V + 11.2306497073) / 11.5254705962)) + 4.26753514993) * (0.262186042981 / (1 + exp((V + 35.8658312707) / (-3.87510627762))) + 0.291755017928); //
			// tau = tau / K_Q10;

			// ac = inf + (ac - inf) * exp(-(dt) / tau);

			// // CNZ_i
			// inf = (IKur_inac_mult * 0.52424) / (1.0 + exp((V + 15.1142 + IKur_inac_shift) / (7.567021 * IKur_inac_grad))) + 0.4580778 + IKur_inac_add;
			// tau = 2328 / (1 + exp(((V) - 9.435) / (3.5827))) + 1739.139;
			// tau = tau / K_Q10;


			// inac = inf + (inac - inf) * exp(-(dt) / tau);
			// I = CNZ_conductance_scaleF * GKur * Cm * CNZ_gkur * (4.5128 + 1.899769 / (1.0 + exp((V - 20.5232) / (-8.26597)))) * ac * inac * (V - EK);
			// I = CNZ_conductance_scaleF * GKur * Cm * CNZ_gkur * (IKur_c + IKur_x0 / (1.0 + exp((V - IKur_y0) / (IKur_K0)))) * ac * inac * (V - EK);

			// I = CNZ_IKur(V,  dt,  state);
			// I = CNZ_IKur_Block(V,  dt,  state, 0);

			 I = Updated_IKur(state, V, dt, 1.0);

			//calculate peak current values
			if ( V == Vstep)
			{
				if (I > peak_current_pos) peak_current_pos = I;
				if (I < peak_current_neg) peak_current_neg = I;
			}
			if ((count % 10) == 0)
			{
				fprintf(current_out, "%f %f %f %f %f\n", global_time, I , V, state[0], state[1]);
			}

			global_time += dt;

			// fprintf(current_out, "%f %f %f\n", t, I / Cm, V);
		} // end time for loop

		// for steady state and tau, include equations here with Vstep as the V term

		fprintf(volt_out, "%d %f %f %f %f\n", i, Vstep, peak_current_pos , peak_current_neg , end_current );
	} // end for V loop

} // end main



