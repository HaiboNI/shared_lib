/*
 * image_data_writer.cc
 * Jonathan D. Stott <jonathan.stott@gmail.com>
 *
 */


//#include "vtkUniformGrid.h"
//#include "vtkXMLImageDataWriter.h"
//#include "vtkUnsignedCharArray.h"
//#include "vtkPointData.h"
#include <stdio.h>
#include <zlib.h>
#include <cstring>
#include <sstream>
#include <string.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>

#define pi 3.14159265358979

using namespace std;

#define NX2 (235)
#define NY2 (269)
#define NZ2 (298)

#define NX (325)
#define NY (325)
#define NZ (425)

#define NF2 8647026

#define MIN (-80.0)
#define MAX (20.0)

unsigned char c[NZ][NY][NX];
unsigned char c2[NZ2][NY2][NX2];

unsigned char c3[NX][NY][NZ];

float * s;
float * s2;


//float s2[NCELL];

float bin(float value);

//int main (int argc, char ** argv)
main()
{

	int argc;
	char *argv;
	int g, h, i, j, k, offset, n, e1, x, y, z, x1, y1, z1;
	int c1 = 100;
	char command[256];
	gzFile gz;
	int count, count2;
	FILE *p, *out;// *outtext *in;
	char * str, *str2, *bin_root, *ere;
	float prue;
	string name2;
	string name;
	FILE *out_wh2;
	float *volt;

	//static float voltf2[NF2];

	double X = 0, Y = 0, Z = 0;
	double R = 0, T = 0, P = 0;
	double x2 = 0, y2 = 0, z2 = 0;
	float ***WholeArray;
	float ***WholeHeart;

	float ***WholeVector;



	volt = new float [NF2];

	WholeArray = new float **[NZ];
	for (x = 0; x < NZ; x++)
	{
		WholeArray[x] = new float*[NY];
		for (y = 0; y < NY; y++)
		{
			WholeArray[x][y] = new float [NZ];
		}
	}

	for (z = 0; z < NZ; z++)
	{
		for (y = 0; y < NY; y++)
		{
			for (x = 0; x < NZ; x++)
			{

				WholeArray[z][y][x] = -100;

			}
		}
	}


	WholeHeart = new float **[NZ2];
	for (x = 0; x < NZ2; x++)
	{
		WholeHeart[x] = new float*[NY2];
		for (y = 0; y < NY2; y++)
		{
			WholeHeart[x][y] = new float [NX2];
		}
	}

	for (z = 0; z < NZ2; z++)
	{
		for (y = 0; y < NY2; y++)
		{
			for (x = 0; x < NX2; x++)
			{

				WholeHeart[z][y][x] = -100;

			}
		}
	}


	WholeVector = new float **[NZ];
	for (x = 0; x < NZ; x++)
	{
		WholeVector[x] = new float*[NY];
		for (y = 0; y < NY; y++)
		{
			WholeVector[x][y] = new float [NZ];
		}
	}

	for (z = 0; z < NZ; z++)
	{
		for (y = 0; y < NY; y++)
		{
			for (x = 0; x < NZ; x++)
			{

				WholeVector[z][y][x] = -100;

			}
		}
	}



	argv = new char [80];
	ere = new char [5];
	bin_root = new char [80];

	str = new char [80];

	cout << "e" << endl;
	gz = gzopen("wholeHeart.geo.gz", "r");
	gzread(gz, c, NZ * NY * NX);
	gzclose(gz);



	cout << "e" << endl;





	count = 0;
	for ( g = 0; g < NZ; g++) {
		for ( h = 0; h < NY; h++) {
			for ( i = 0 ; i < NX; i++) {
				if (c[g][h][i] != 0 )
				{

					count ++;
					WholeVector[g][h][i] = c[g][h][i];
				}


			}
		}
	}



	s = new float [count];


	gz = gzopen("ATRIUM_cleaned.mat.gz", "r");
	gzread(gz, c2, NZ2 * NY2 * NX2);
	gzclose(gz);

	cout << "e" << endl;




	count2 = 0;
	for ( g = 0; g < NZ2; g++) {
		for ( h = 0; h < NY2; h++) {
			for ( i = 0 ; i < NX2; i++) {
				if (c2[g][h][i] != 0 )
				{

					count2 ++;
					WholeHeart[g][h][i] = c2[g][h][i];
				}


			}
		}
	}


	cout << count2 << endl;


	s2 = new float [count2];


//Inicialize the for cicle

	for (e1 = 465; e1 < 550; e1++)
	{

		count = 0;
		for ( g = 0; g < NZ; g++) {
			for ( h = 0; h < NY; h++) {
				for ( i = 0 ; i < NX; i++) {
					if (c[g][h][i] != 0 )
					{

						count ++;
						WholeVector[g][h][i] = c[g][h][i];
					}


				}
			}
		}


		cout << count << endl;


		count2 = 0;
		for ( g = 0; g < NZ2; g++) {
			for ( h = 0; h < NY2; h++) {
				for ( i = 0 ; i < NX2; i++) {
					if (c2[g][h][i] != 0 )
					{

						count2 ++;
						WholeHeart[g][h][i] = c2[g][h][i];
					}


				}
			}
		}


		cout << count2 << endl;



		sprintf(bin_root, "/home/e/Desktop/UMIPWildType/v%04d.bin.xz", e1);
		cout << bin_root << endl;


		sprintf(command, "xzcat %s", bin_root);
		p = popen(command, "r");
		fread(s, sizeof(float), count, p);
		pclose(p);


		cout << "e" << endl;
		n = 0;
		g = 0;
		for (z = 0; z < NZ; z++) {

			for (y = 0; y < NY; y++) {
				for (x = 0; x < NX; x++) {


					if (WholeVector[z][y][x] != -100 )
					{
						WholeVector[z][y][x] = s[n];

						if (s[n] == 0)
						{
							g++;
						}

						n++;

					}

				}
			}
		}




		cout << "e " << n << " " << g << endl;





//print the first output
		/*

		sprintf (str, "hhA%04d.vtk",e1);

		n=0;
		ofstream out_rot;

		        out_rot.open(str);

			out_rot << "# vtk DataFile Version 3.0" << endl;
			out_rot << "vtk output" << endl;
			out_rot << "ASCII" << endl;
			out_rot << "DATASET STRUCTURED_POINTS" << endl;
			out_rot << "DIMENSIONS " << NX << " " << NY << " " << NZ << endl;
			out_rot << "SPACING 0.00033 0.00033 0.00033" << endl;
			out_rot << "ORIGIN 0 0 0" << endl;
			out_rot << "POINT_DATA " << NX*NY*NZ << endl;
			out_rot << "SCALARS ImageFile int 1" << endl;
			out_rot << "LOOKUP_TABLE default" << endl;

			for (z = 0; z < NZ; z++){

				for (y = 0; y < NY; y++){
					for (x = 0; x < NX; x++){


		out_rot<<(int)WholeVector[z][y][x]<<" ";

					}
				out_rot << endl;
				}
			}

			out_rot.clear();
			out_rot.close();
			cout << "Could not open File11 " <<n << endl;
		*/











		n = 0;
		g = 0;
		for (z = 0; z < NZ; z++) {

			for (y = 0; y < NY; y++) {
				for (x = 0; x < NX; x++) {


					if (c[z][y][x] < 10 )
					{
						WholeVector[z][y][x] = -100;

						n++;

					}
					else
					{
						g++;
					}

				}
			}
		}





		cout << count << " e " << n << " " << g << endl;





//Inicialize the rotation/translation

		for (z = 0; z < NZ; z++) {
			for (y = 0; y < NY; y++) {
				for (x = 0; x < NX; x++) {

					X = x;
					Y = y;
					Z = z;

					//convert to z-axis cylindrical polar co-ordinates
					R = sqrt(X * X + Y * Y);
					T = atan(Y / X);
					if (X == 0) {
						T = 0;
					}
					Z = Z;

					//Check for correct quadrant
					if (X < 0 && Y >= 0) {
						T = T + pi;
					}
					if (X < 0 && Y < 0) {
						T = T - pi;
					}

					//cylindrical polar transformations here
					R = R + (c1 / 100) * 0;
					T = T + (c1 / 100) * 0 * (pi); // -0.25
					Z = Z;

					//convert from cylindrical polar to cartesian coordinates
					X = (R * cos(T));
					Y = (R * sin(T));
					Z = Z;

					//convert to X-axis cylindrical polar co-ordinates
					R = sqrt(X * X + Z * Z);
					T = atan(Z / X);
					if (X == 0) {
						T = 0;
					}
					Y = Y;

					//Check for correct quadrant
					if (X < 0 && Z >= 0) {
						T = T + pi;
					}
					if (X < 0 && Z < 0) {
						T = T - pi;
					}

					//cylindrical polar transformations here
					R = R + (c1 / 100) * 0;
					T = T + (c1 / 100) * (0.5) * (pi); // -0.25
					Y = Y;

					//convert from cylindrical polar to cartesian coordinates
					X = (R * cos(T));
					Z = (R * sin(T));
					Y = Y;

					//convert to Y-axis cylindrical polar co-ordinates
					R = sqrt(Y * Y + Z * Z);
					T = atan(Y / Z);
					if (Z == 0) {
						T = 0;
					}
					X = X;

					//Check for correct quadrant
					if (Z < 0 && Y >= 0) {
						T = T + pi;
					}
					if (Z < 0 && Y < 0) {
						T = T - pi;
					}

					//cylindrical polar transformations here
					R = R + (c1 / 100) * 0;
					T = T + (c1 / 100) * (1) * (pi); // -0.25
					X = X;

					//convert from cylindrical polar to cartesian coordinates
					Z = (R * cos(T));
					Y = (R * sin(T));
					X = X;




					//cartesian transformations here

					X = ((1 - ((c1 / 100) * 0)) * X) + (c1 / 100) * 0; //10
					Y = ((1 - ((c1 / 100) * 0)) * Y) + (c1 / 100) * 0; //-60
					Z = ((1 - ((c1 / 100) * 0)) * Z) + (c1 / 100) * 0; //10
					X = X + 425;
					Y = Y + 325;
					Z = Z + 325;


					x1 = (int)round(X);
					y1 = (int)round(Y);
					z1 = (int)round(Z);



					if (x1 < NZ && x1 > 0 && y1 > 0 && y1 < NY && z1 > 0 && z1 < NZ)
					{

						WholeArray[z1][y1][x1] = WholeVector[z][y][x];
					}


				}
			}
		}


		cout << "Could not open File5" << endl;



//print to compare the aoutput


		sprintf (str, "hnwh%04d.vtk", e1);




		n = 0;
		ofstream out_wh1;


		out_wh1.open(str);

		out_wh1 << "# vtk DataFile Version 3.0" << endl;
		out_wh1 << "vtk output" << endl;
		out_wh1 << "ASCII" << endl;
		out_wh1 << "DATASET STRUCTURED_POINTS" << endl;
		out_wh1 << "DIMENSIONS " << NX << " " << NY << " " << NZ << endl;
		out_wh1 << "SPACING 0.00033 0.00033 0.00033" << endl;
		out_wh1 << "ORIGIN 0 0 0" << endl;
		out_wh1 << "POINT_DATA " << NX*NY*NZ << endl;
		out_wh1 << "SCALARS ImageFile int 1" << endl;
		out_wh1 << "LOOKUP_TABLE default" << endl;



		for (z = 0; z < NZ; z++) {

			for (y = 0; y < NY; y++) {
				for (x = 0; x < NX; x++) {


					out_wh1 << (int)WholeArray[z][y][x] << " ";

				}
				out_wh1 << endl;
			}

		}

		out_wh1.clear();
		out_wh1.close();

		cout << n << endl;



		sprintf(bin_root, "/home/e/Desktop/Atrial_results/Control_SAN/v%04d.bin.xz", e1);
		cout << bin_root << endl;

		sprintf(command, "xzcat %s", bin_root);
		p = popen(command, "r");
		fread(s2, sizeof(float), count2, p);
		pclose(p);



		g = 0;
		for (z = 0; z < NZ2; z++) {

			for (y = 0; y < NY2; y++) {
				for (x = 0; x < NX2; x++) {
					if (c2[z][y][x] != 0)
					{
						WholeHeart[z][y][x] = s2[g];
						g++;
					}

				}
			}
		}


		g = 0;


		for (z = 0; z < NZ2; z++) {

			for (y = 0; y < NY2; y++) {
				for (x = 0; x < NX2; x++) {



					if (c2[z][y][x] != 0)
					{

						WholeArray[z][y][x] = WholeHeart[z][y][x];
					}

					if (c2[z][y][x] != 0 && WholeArray[z][y][x] != -100)
					{
//cout<<x <<" "<<y<<" "<<z<<endl;
						g++;
					}
				}
			}
		}


		cout << g << endl;

		i = 0;
		j = 0;
		for (z = 0; z < NZ; z++)
		{
			for (y = 0; y < NY; y++)
			{
				for (x = 0; x < NX; x++)
				{

					if (WholeArray[z][y][x] != -100)
					{
						i++;
					}
					if (c2[z][y][x] != -100)
					{
						j++;
					}
				}



			}
		}





		/*
		cout<<"e2"<<endl;

		sprintf (str, "hwhAMi%04d.vtk",e1);

		n=0;
		ofstream out_whA;


		        out_whA.open(str);

		        out_whA << "# vtk DataFile Version 3.0" << endl;
		        out_whA << "vtk output" << endl;
		        out_whA << "ASCII" << endl;
		        out_whA << "DATASET STRUCTURED_POINTS" << endl;
		        out_whA << "DIMENSIONS " << NZ << " " << NY << " " << NX << endl;
		        out_whA << "SPACING 0.00033 0.00033 0.00033" << endl;
		        out_whA << "ORIGIN 0 0 0" << endl;
		        out_whA << "POINT_DATA " << NX*NY*NZ << endl;
		        out_whA << "SCALARS ImageFile int 1" << endl;
		        out_whA << "LOOKUP_TABLE default" << endl;



		        for (z = 0; z < NX; z++){

		                for (y = 0; y < NY; y++){
		                        for (x = 0; x < NZ; x++){


		out_whA <<(int)WholeArray[z][y][x] << " ";


		                        }
		               out_whA << endl;
		                }
		               out_whA << endl;
		        }

			out_whA.clear();
		        out_whA.close();


		/*
		sprintf(bin_root,"/home/e/Desktop/soloatria/SAN/v%04d.bin.xz",e1);
		cout <<bin_root<<endl;

		    sprintf(command, "xzcat %s", bin_root);
		    p = popen(command, "r");
		    fread(s2, sizeof(float), count2, p);
		    pclose(p);
		*/



		cout << i << "  " << j << " " << count << " " << count2 << " " << n << endl;
		k = 0;
		for (z = 0; z < NX; z++)
		{
			for (y = 0; y < NY; y++)
			{
				for (x = 0; x < NZ; x++)
				{
					if (WholeArray[z][y][x] == -100)
					{
						c3[z][y][x] = 0;
					}
					else
					{
						c3[z][y][x] = 1;
						k++;

					}
				}
			}
		}

		cout << "e " << k << endl;
		gz = gzopen("wholeHeartAtriaMike.geo.gz", "wb9");
		gzwrite(gz, c3, NX * NY * NZ);
		gzclose(gz);

		cout << "e" << endl;


		k = 0;

		for (z = 0; z < NX; z++)
		{
			for (y = 0; y < NY; y++)
			{
				for (x = 0; x < NZ; x++)
				{
					if (WholeArray[z][y][x] != -100)
					{
						volt[k] = WholeArray[z][y][x];
//cout <<k<<endl;

						k++;

					}

				}
			}
		}

		cout << k << endl;
		cout << "e2" << endl;

		str2 = new char [80];



		sprintf(str, "binWhMAtrMi/v%04d.bin", e1);

		cout << str << endl;

		ofstream outbin;

		outbin.open(str);


		for (i = 0; i < NF2; i++)
		{

			if (volt[i] > -100 )
			{

				outbin << volt[i] << endl;

			}
		}

		cout << " r2 " << i << volt[i] << endl;


		//outbin.write( (char*)&voltf2, sizeof( voltf2 ) );

		outbin.clear();
		outbin.close();











	}




	delete [] s;
	delete [] s2;
	delete [] volt;











	for (z = 0; z < NZ; ++z)
	{
		for (y = 0; y < NY; ++y)
		{
			delete [] WholeArray[z][y];

		}
		delete [] WholeArray[z];


	}
	delete [] WholeArray;




	for (z = 0; z < NZ2; ++z)
	{
		for (y = 0; y < NY2; ++y)
		{
			delete [] WholeHeart[z][y];

		}
		delete [] WholeHeart[z];


	}
	delete [] WholeHeart;



	for (z = 0; z < NZ; ++z)
	{
		for (y = 0; y < NY; ++y)
		{
			delete [] WholeVector[z][y];

		}
		delete [] WholeVector[z];


	}
	delete [] WholeVector;




	return 0;

} /* end of main() */

float bin(float value) {
	if (value >= MAX) {
		return 254;
	} else if (value <= MIN) {
		return 0;
	} else {
		return (unsigned char) (((value - MIN) / (MAX - MIN)) * 254);
	}
}

